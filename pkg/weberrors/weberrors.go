package weberrors

import "github.com/pkg/errors"

var (
	ErrNotFound     = errors.New("requested item is not found")
	ErrUnauthorized = errors.New("unauthorized access")
	ErrConflict     = errors.New("conflict with the current state of the resource")
	ErrBadRequest   = errors.New("malformed request")
	ErrForbidden    = errors.New("you are not allowed to perform this action")
)

type Err struct {
	Error string `json:"error"`
}

//type NotFound struct {
//	Msg string
//}
//
//func (e NotFound) Error() string {
//	return e.Msg
//}
//
//type Unauthorized struct {
//	Msg string
//}
//
//func (e Unauthorized) Error() string {
//	return e.Msg
//}
//
//type Conflict struct {
//	Msg string
//}
//
//func (e Conflict) Error() string {
//	return e.Msg
//}
//
//type BadRequest struct {
//	Msg string
//}
//
//func (e BadRequest) Error() string {
//	return e.Msg
//}
//
//type Forbidden struct {
//	Msg string
//}
//
//func (e Forbidden) Error() string {
//	return e.Msg
//}
