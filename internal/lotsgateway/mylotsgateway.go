package lotsgateway

import (
	"context"
	"encoding/json"
	"io"
	"log"
	"time"

	"gitlab.com/illfalcon/tfs-go-cp/pkg/weberrors"

	lotsinteractionspb "gitlab.com/illfalcon/tfs-go-cp/internal/rpc/lotsinteractions"

	"gitlab.com/illfalcon/tfs-go-cp/internal/lot"
	"gitlab.com/illfalcon/tfs-go-cp/internal/user"

	"github.com/pkg/errors"
	"gitlab.com/illfalcon/tfs-go-cp/internal/auth"
	lotsquerypb "gitlab.com/illfalcon/tfs-go-cp/internal/rpc/lotsquery"
	"gitlab.com/illfalcon/tfs-go-cp/internal/session"
)

type MyLotsGatewayService struct {
	SessionService  session.Service
	GRPCQueryClient lotsquerypb.LotsQueryServiceClient
	GRPCInterClient lotsinteractionspb.LotsInteractionsServiceClient
}

func lotQueryPBToLot(l *lotsquerypb.Lot) lot.Lot {
	c := user.ShortUser{
		ID:        int(l.Owner.ID),
		FirstName: l.Owner.FirstName,
		LastName:  l.Owner.LastName,
	}
	b := user.ShortUser{
		ID:        int(l.Buyer.ID),
		FirstName: l.Buyer.FirstName,
		LastName:  l.Buyer.LastName,
	}
	endAt, _ := time.Parse(time.RFC3339, l.EndAt)
	lot := lot.Lot{
		ID:          int(l.ID),
		Creator:     c,
		Buyer:       b,
		Title:       l.Title,
		Description: l.Description,
		BuyPrice:    l.BuyPrice,
		MinPrice:    l.MinPrice,
		PriceStep:   l.PriceStep,
		EndAt:       endAt,
		Status:      l.Status,
	}
	return lot
}

// nolint: dupl
func lotInterPBToLot(l *lotsinteractionspb.Lot) lot.Lot {
	c := user.ShortUser{
		ID:        int(l.Owner.ID),
		FirstName: l.Owner.FirstName,
		LastName:  l.Owner.LastName,
	}
	b := user.ShortUser{
		ID:        int(l.Buyer.ID),
		FirstName: l.Buyer.FirstName,
		LastName:  l.Buyer.LastName,
	}
	endAt, _ := time.Parse(time.RFC3339, l.EndAt)
	lot := lot.Lot{
		ID:          int(l.ID),
		Creator:     c,
		Buyer:       b,
		Title:       l.Title,
		Description: l.Description,
		BuyPrice:    l.BuyPrice,
		MinPrice:    l.MinPrice,
		PriceStep:   l.PriceStep,
		EndAt:       endAt,
		Status:      l.Status,
	}
	return lot
}

func (qs MyLotsGatewayService) Lot(id int, token string) ([]byte, error) {
	_, err := auth.AuthenticateToken(token, qs.SessionService)
	if err != nil {
		return nil, errors.Wrap(err, "error in query.Lot()")
	}
	req := lotsquerypb.LotRequest{
		Id: int64(id),
	}
	resp, err := qs.GRPCQueryClient.Lot(context.Background(), &req)
	lot := lotQueryPBToLot(resp.Lot)
	if err != nil {
		return nil, errors.Wrapf(err, "error in lotsgateway.Lot(), Lot with id %v", id)
	}
	lotJSON, err := json.Marshal(lot)
	if err != nil {
		return nil, errors.Wrapf(err, "error while JSON encoding Lot %v in lotsgateway.Lot()", id)
	}
	return lotJSON, nil

}

func (qs MyLotsGatewayService) LotsOfUser(userID int, token string, lotType string) ([]byte, error) {
	_, err := auth.AuthenticateToken(token, qs.SessionService)
	if err != nil {
		return nil, errors.Wrap(err, "error in lotsgateway.LotsOfUser()")
	}
	req := lotsquerypb.LotsOfUserRequest{
		UserId:  int64(userID),
		LotType: lotType,
	}
	resp, err := qs.GRPCQueryClient.LotsOfUser(context.Background(), &req)
	if err != nil {
		return nil, errors.Wrap(err, "error in lotsgateway.LotsOfUser()")
	}
	var ll []lot.Lot
	for {
		lResp, e := resp.Recv()
		if e != nil {
			if e == io.EOF {
				break
			}
			return nil, errors.Wrap(e, "error in lotsgateway.LotsOfUser()")
		}
		ll = append(ll, lotQueryPBToLot(lResp.Lot))
	}
	lotsJSON, err := json.Marshal(ll)
	if err != nil {
		return nil, errors.Wrap(err, "error while marshalling in query.LotsOfUser()")
	}
	return lotsJSON, nil
}

func (qs MyLotsGatewayService) Lots(token string, status string) ([]byte, error) {
	_, err := auth.AuthenticateToken(token, qs.SessionService)
	if err != nil {
		return nil, errors.Wrap(err, "error in lotsgateway.Lots()")
	}
	req := lotsquerypb.LotsRequest{
		Status: status,
	}
	resp, err := qs.GRPCQueryClient.Lots(context.Background(), &req)
	if err != nil {
		return nil, errors.Wrap(err, "error in lotsgateway.Lots()")
	}
	var ll []lot.Lot
	for {
		lResp, e := resp.Recv()
		if e != nil {
			if e == io.EOF {
				break
			}
			return nil, errors.Wrap(e, "error in lotsgateway.Lots()")
		}
		ll = append(ll, lotQueryPBToLot(lResp.Lot))
	}
	lotsJSON, err := json.Marshal(ll)
	if err != nil {
		return nil, errors.Wrap(err, "error while marshalling in lotsgateway.Lots()")
	}
	return lotsJSON, nil
}

func (qs MyLotsGatewayService) NewLot(title, description string, minPrice, priceStep float64, endAt time.Time, bearerToken string) ([]byte, error) {
	creatorID, err := auth.AuthenticateToken(bearerToken, qs.SessionService)
	if err != nil {
		return nil, errors.Wrap(err, "error in authentication in lotsgateway.NewLot()")
	}
	endAtString := endAt.Format(time.RFC3339)
	req := lotsinteractionspb.NewLotRequest{Title: title, Description: description, MinPrice: minPrice,
		PriceStep: priceStep, EndAt: endAtString, CreatorId: int64(creatorID)}
	l, err := qs.GRPCInterClient.NewLot(context.Background(), &req)
	if err != nil {
		return nil, errors.Wrap(err, "error in NewLot()")
	}
	log.Printf("%+v\n", l)
	lotJSON, err := json.Marshal(lotInterPBToLot(l.Lot))
	if err != nil {
		return nil, errors.Wrap(err, "error in lotsgateway.NewLot()")
	}
	return lotJSON, nil
}

func (qs MyLotsGatewayService) UpdateLot(lotID int, title, description string, minPrice, priceStep float64,
	status string, endAt time.Time, bearerToken string) ([]byte, error) {
	updaterID, err := auth.AuthenticateToken(bearerToken, qs.SessionService)
	if err != nil {
		return nil, errors.Wrap(err, "error in authentication in lotsgateway.UpdateLot()")
	}
	lReq := lotsquerypb.LotRequest{Id: int64(lotID)}
	pbLot, err := qs.GRPCQueryClient.Lot(context.Background(), &lReq)
	if err != nil {
		return nil, errors.Wrap(err, "error in lotsgateway.UpdateLot()")
	}
	if int(pbLot.Lot.Owner.ID) != updaterID {
		return nil, errors.Wrap(weberrors.ErrForbidden,
			"forbidden access: lot belongs to another user in lotsgateway.UpdateWay()")
	}
	endAtStr := endAt.Format(time.RFC3339)
	updReq := lotsinteractionspb.UpdateLotRequest{Title: title, Description: description,
		MinPrice: minPrice, PriceStep: priceStep, EndAt: endAtStr, Lot_ID: int64(lotID), Status: status}
	newLot, err := qs.GRPCInterClient.UpdateLot(context.Background(), &updReq)
	if err != nil {
		return nil, errors.Wrap(err, "error in lotsgateway.UpdateLot()")
	}
	JSONLot, err := json.Marshal(lotInterPBToLot(newLot.Lot))
	if err != nil {
		return nil, errors.Wrap(err, "error in lotsgateway.UpdateLot()")
	}
	return JSONLot, nil
}

func (qs MyLotsGatewayService) BuyLot(lotID int, price float64, bearerToken string) ([]byte, error) {
	buyerID, err := auth.AuthenticateToken(bearerToken, qs.SessionService)
	if err != nil {
		return nil, errors.Wrap(err, "error in lotsgateway.BuyLot()")
	}
	req := lotsinteractionspb.BuyLotRequest{Lot_ID: int64(lotID), Buyer_ID: int64(buyerID), Price: price}
	newLot, err := qs.GRPCInterClient.BuyLot(context.Background(), &req)
	if err != nil {
		return nil, errors.Wrap(err, "error in lotsgateway.BuyLot()")
	}
	JSONLot, err := json.Marshal(lotInterPBToLot(newLot.Lot))
	if err != nil {
		return nil, errors.Wrap(err, "error in lotsgateway.BuyLot()")
	}
	return JSONLot, nil
}

func (qs MyLotsGatewayService) EndTrades() ([][]byte, error) {
	em := lotsinteractionspb.EmptyRequest{}
	resp, err := qs.GRPCInterClient.EndLots(context.Background(), &em)
	if err != nil {
		return nil, errors.Wrap(err, "error in lotsgateway.EndTrades()")
	}
	var ll []lot.Lot
	for {
		lResp, err := resp.Recv()
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, errors.Wrap(err, "error in lotsgateway.EndTrades()")
		}
		ll = append(ll, lotInterPBToLot(lResp.Lot))
	}
	var jsonLots [][]byte
	for _, l := range ll {
		jsonLot, err := json.Marshal(l)
		if err != nil {
			return nil, errors.Wrap(err, "error in lotsgateway.EndTrades()")
		}
		jsonLots = append(jsonLots, jsonLot)
	}
	return jsonLots, nil

}

func (qs MyLotsGatewayService) DeleteLot(lotID int, bearerToken string) error {
	buyerID, err := auth.AuthenticateToken(bearerToken, qs.SessionService)
	if err != nil {
		return errors.Wrap(err, "error in lotsgateway.DeleteLot()")
	}
	req := lotsinteractionspb.DeleteLotRequest{LotId: int64(lotID), DeleterId: int64(buyerID)}
	_, err = qs.GRPCInterClient.DeleteLot(context.Background(), &req)
	if err != nil {
		return errors.Wrap(err, "error in lotsgateway.DeleteLot()")
	}
	return nil
}
