package lotsgateway

import "time"

type LotsQuerier interface {
	Lot(id int, token string) ([]byte, error)
	LotsOfUser(userID int, token string, lotType string) ([]byte, error)
	Lots(token string, status string) ([]byte, error)
}

type LotsInteractor interface {
	NewLot(title, description string, minPrice, priceStep float64, endAt time.Time, bearerToken string) ([]byte, error)
	UpdateLot(lotID int, title, description string, minPrice, priceStep float64, status string, endAt time.Time, bearerToken string) ([]byte, error)
	DeleteLot(lotID int, bearerToken string) error
	BuyLot(lotID int, price float64, bearerToken string) ([]byte, error)
	EndTrades() ([][]byte, error)
}

type Service interface {
	LotsQuerier
	LotsInteractor
}
