////Package to create objects of business logic and pass them to storage from user input.
package interactions

//
//import (
//	"encoding/json"
//	"time"
//
//	"github.com/pkg/errors"
//	"gitlab.com/illfalcon/tfs-go-cp/internal/auth"
//	"gitlab.com/illfalcon/tfs-go-cp/internal/lot"
//	"gitlab.com/illfalcon/tfs-go-cp/internal/session"
//	"gitlab.com/illfalcon/tfs-go-cp/internal/user"
//	"gitlab.com/illfalcon/tfs-go-cp/pkg/weberrors"
//)
//
//type MyInteractionService struct {
//	SessionService session.Service
//	UserService    user.Service
//	LotService     lot.Service
//}
//
//func (mis *MyInteractionService) NewLot(title, description string, minPrice, priceStep float64, endAt time.Time, bearerToken string) error {
//	creatorID, err := auth.AuthenticateToken(bearerToken, mis.SessionService)
//	if err != nil {
//		return errors.Wrap(err, "error in authentication in NewLot()")
//	}
//	lotObj := lot.Lot{Title: title, Description: description, MinPrice: minPrice, PriceStep: priceStep, EndAt: endAt}
//	err = mis.LotService.CreateLot(lotObj, creatorID)
//	if err != nil {
//		return errors.Wrap(err, "error in NewLot()")
//	}
//	return nil
//}
//
//func (mis *MyInteractionService) UpdateLot(lotID int, title, description string, minPrice, priceStep float64, status string, endAt time.Time, bearerToken string) ([]byte, error) {
//	updaterID, err := auth.AuthenticateToken(bearerToken, mis.SessionService)
//	if err != nil {
//		return nil, errors.Wrap(err, "error in authentication in UpdateLot()")
//	}
//	dbLot, err := mis.LotService.Lot(lotID)
//	if err != nil {
//		return nil, errors.Wrap(err, "error in DeleteLot()")
//	}
//	if dbLot.Creator.ID != updaterID {
//		return nil, errors.Wrap(weberrors.Unauthorized{Msg: "error in UpdateLot()"},
//			"unauthorized access: lot belongs to another user")
//	}
//	lotObj := lot.Lot{Title: title, Description: description, MinPrice: minPrice, PriceStep: priceStep, EndAt: endAt,
//		Status: status}
//	newLot, err := mis.LotService.UpdateLot(lotID, lotObj)
//	if err != nil {
//		return nil, errors.Wrap(err, "error in UpdateLot()")
//	}
//	JSONLot, err := json.Marshal(newLot)
//	if err != nil {
//		return nil, errors.Wrap(err, "error in interactions.UpdateLot()")
//	}
//	return JSONLot, nil
//}
//
//func (mis *MyInteractionService) BuyLot(lotID int, price float64, bearerToken string) ([]byte, error) {
//	buyerID, err := auth.AuthenticateToken(bearerToken, mis.SessionService)
//	if err != nil {
//		return nil, errors.Wrap(err, "error in interactions.BuyLot()")
//	}
//	newLot, err := mis.LotService.BuyLot(lotID, buyerID, price)
//	if err != nil {
//		return nil, errors.Wrap(err, "error in interactions.BuyLot()")
//	}
//	JSONLot, err := json.Marshal(newLot)
//	if err != nil {
//		return nil, errors.Wrap(err, "error in interactions.BuyLot()")
//	}
//	return JSONLot, nil
//}
//
//func (mis *MyInteractionService) EndTrades() ([][]byte, error) {
//	ll, err := mis.LotService.EndTrades()
//	if err != nil {
//		return nil, errors.Wrap(err, "error in interactions.EndTrades()")
//	}
//	var jsonLots [][]byte
//	for _, l := range ll {
//		jsonLot, err := json.Marshal(l)
//		if err != nil {
//			return nil, errors.Wrap(err, "error in interactions.EndTrades()")
//		}
//		jsonLots = append(jsonLots, jsonLot)
//	}
//	return jsonLots, nil
//}
//
////func (mis *MyInteractionService) DeleteLot(lotID int, bearerToken string) error {
////	deleterID, err := auth.AuthenticateToken(bearerToken, mis.SessionService)
////	if err != nil {
////		return errors.Wrap(err, "error in authentication in DeleteLot()")
////	}
////	dbLot, err := mis.LotService.Lot(lotID)
////	if err != nil {
////		return errors.Wrap(err, "error in DeleteLot()")
////	}
////	if deleterID != dbLot.CreatorID {
////		return errors.Wrap(weberrors.Unauthorized{Msg: "error in DeleteLot()"},
////			"unauthorized access: lot belongs to another user")
////	}
////	err = mis.LotService.DeleteLot(lotID)
////	if err != nil {
////		return errors.Wrap(err, "error in DeleteUser()")
////	}
////	return nil
////}
