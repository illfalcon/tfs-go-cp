package inmemorydb

import (
	"time"

	"github.com/pkg/errors"
	"gitlab.com/illfalcon/tfs-go-cp/internal/lot"
	"gitlab.com/illfalcon/tfs-go-cp/internal/session"
	"gitlab.com/illfalcon/tfs-go-cp/internal/user"
	"gitlab.com/illfalcon/tfs-go-cp/pkg/weberrors"
)

type User struct {
	ID         int
	FirstName  string
	LastName   string
	EMail      string
	Sessions   []session.Session
	Lots       []lot.Lot
	BoughtLots []lot.Lot
	Password   string
	Birthday   *time.Time
	CreatedAt  time.Time
}

type UserService struct {
	Storage map[int]User
	counter int
}

func NewUserService() UserService {
	return UserService{
		Storage: map[int]User{},
		counter: 1,
	}
}

//func dbUserToShortUser(u User) user.ShortUser {
//	return user.ShortUser{ID: u.ID, FirstName: u.FirstName, LastName: u.LastName}
//}

//func queryUserFromDB(db *gorm.DB, id int) (*User, error) {
//	var dbUser User
//	result := db.First(&dbUser, id)
//	if result.RecordNotFound() {
//		return nil, errors.Wrapf(weberrors.ErrNotFound,
//			"user with id %v not found", id)
//	}
//	if result.Error != nil {
//		return nil, errors.Wrapf(result.Error, "error in method queryUserFromDB() while searching for user %v", id)
//	}
//	return &dbUser, nil
//}

func checkUniqueEmail(storage map[int]User, email string) error {
	_, err := queryUserFromDBByEmail(storage, email)
	if err == nil {
		return weberrors.ErrConflict
	}
	return nil
}

func queryUserFromDBByEmail(storage map[int]User, email string) (User, error) {
	for _, u := range storage {
		if u.EMail == email {
			return u, nil
		}
	}
	return User{}, weberrors.ErrNotFound
}

func (us UserService) User(id int) (*user.User, error) {
	return &user.User{}, nil
}

func (us UserService) UserByEmailPassword(email, password string) (*user.User, error) {
	return &user.User{}, nil
}

func (us *UserService) CreateUser(u user.User, password string) error {
	err := checkUniqueEmail(us.Storage, u.EMail)
	if err != nil {
		return errors.Wrap(err, "error in inmemorydb.CreateUser()")
	}
	dbUser := User{ID: us.counter, FirstName: u.FirstName, LastName: u.LastName, Birthday: &u.Birthday,
		EMail: u.EMail, Password: password, CreatedAt: time.Now()}
	us.Storage[us.counter] = dbUser
	us.counter++
	return nil
}

func (us *UserService) UpdateUser(id int, u user.User) (*user.User, error) {
	return nil, nil
}
