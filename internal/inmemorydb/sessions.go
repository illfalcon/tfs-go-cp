package inmemorydb

import (
	"time"

	"gitlab.com/illfalcon/tfs-go-cp/internal/session"
)

type Session struct {
	SessionID  string
	UserID     int
	CreatedAt  time.Time
	ValidUntil time.Time
}

type SessionService struct {
	storage map[string]Session
}

func NewSessionService() SessionService {
	return SessionService{
		storage: map[string]Session{},
	}
}

func (ss SessionService) Session(id string) (*session.Session, error) {
	return &session.Session{}, nil
}

func (ss *SessionService) CreateSession(s session.Session) error {
	return nil
}
