package lotsinteractions

import (
	"time"

	lotsinteractionspb "gitlab.com/illfalcon/tfs-go-cp/internal/rpc/lotsinteractions"
)

type LotInteractor interface {
	NewLot(title, description string, minPrice, priceStep float64,
		endAt time.Time, creatorID int) (*lotsinteractionspb.Lot, error)
	UpdateLot(lotID int, title, description string, minPrice,
		priceStep float64, status string, endAt time.Time) (*lotsinteractionspb.Lot, error)
	//DeleteLot(lotID int, bearerToken string) error
	BuyLot(lotID int, price float64, buyerID int) (*lotsinteractionspb.Lot, error)
	EndLots() ([]*lotsinteractionspb.Lot, error)
	DeleteLot(lotID int, deleterID int) error
}

type Service interface {
	LotInteractor
}
