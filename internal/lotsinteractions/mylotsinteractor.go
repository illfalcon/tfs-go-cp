package lotsinteractions

import (
	"log"
	"time"

	"github.com/pkg/errors"
	lotsinteractionspb "gitlab.com/illfalcon/tfs-go-cp/internal/rpc/lotsinteractions"

	"gitlab.com/illfalcon/tfs-go-cp/internal/lot"
)

type MyLotsInteractionsService struct {
	LotService lot.Service
}

func lotToLotPB(l lot.Lot) *lotsinteractionspb.Lot {
	owner := &lotsinteractionspb.ShortUser{
		ID: int64(l.Creator.ID), FirstName: l.Creator.FirstName, LastName: l.Creator.LastName,
	}
	buyer := &lotsinteractionspb.ShortUser{
		ID: int64(l.Buyer.ID), FirstName: l.Buyer.FirstName, LastName: l.Buyer.LastName,
	}
	endAtStr := l.EndAt.Format(time.RFC3339)
	log.Println(endAtStr)
	lotPB := &lotsinteractionspb.Lot{
		ID: int64(l.ID), Title: l.Title, Description: l.Description, PriceStep: l.PriceStep,
		MinPrice: l.MinPrice, BuyPrice: l.BuyPrice, Owner: owner, Buyer: buyer, EndAt: endAtStr, Status: l.Status,
	}
	return lotPB
}

func (mli *MyLotsInteractionsService) NewLot(title, description string, minPrice,
	priceStep float64, endAt time.Time, creatorID int) (*lotsinteractionspb.Lot, error) {
	l := lot.Lot{Title: title, Description: description, MinPrice: minPrice, PriceStep: priceStep, EndAt: endAt}
	newLot, err := mli.LotService.CreateLot(l, creatorID)
	if err != nil {
		return nil, errors.Wrap(err, "error in lotsinteractions.NewLot()")
	}
	return lotToLotPB(*newLot), nil
}

func (mli *MyLotsInteractionsService) UpdateLot(lotID int, title, description string,
	minPrice, priceStep float64, status string, endAt time.Time) (*lotsinteractionspb.Lot, error) {
	l := lot.Lot{Title: title, Description: description, MinPrice: minPrice, PriceStep: priceStep, EndAt: endAt, Status: status}
	updLot, err := mli.LotService.UpdateLot(lotID, l)
	if err != nil {
		return nil, errors.Wrap(err, "error in lotsinteractions.UpdateLot()")
	}
	return lotToLotPB(*updLot), nil
}

func (mli *MyLotsInteractionsService) BuyLot(lotID int, price float64, buyerID int) (*lotsinteractionspb.Lot, error) {
	boughtLot, err := mli.LotService.BuyLot(lotID, buyerID, price)
	if err != nil {
		return nil, errors.Wrap(err, "error in lotsinteractions.BuyLot()")
	}
	return lotToLotPB(*boughtLot), nil
}

func (mli *MyLotsInteractionsService) EndLots() ([]*lotsinteractionspb.Lot, error) {
	ll, err := mli.LotService.EndTrades()
	if err != nil {
		return nil, errors.Wrap(err, "error in lotsinteractions.EndLots()")
	}
	var lots []*lotsinteractionspb.Lot
	for _, l := range ll {
		lots = append(lots, lotToLotPB(*l))
	}
	return lots, nil
}

func (mli *MyLotsInteractionsService) DeleteLot(lotID int, deleterID int) error {
	err := mli.LotService.DeleteLot(lotID, deleterID)
	if err != nil {
		return errors.Wrap(err, "error in lotsinteractions.DeleteLot()")
	}
	return nil
}
