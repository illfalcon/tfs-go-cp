//Package transforms objects of in-app business logic into JSON-encoded objects
package lotsquery

import (
	lotsquerypb "gitlab.com/illfalcon/tfs-go-cp/internal/rpc/lotsquery"
)

type LotQuerier interface {
	Lot(id int) (*lotsquerypb.Lot, error)
	LotsOfUser(userID int, lotType string) ([]*lotsquerypb.Lot, error)
	Lots(status string) ([]*lotsquerypb.Lot, error)
}

type Service interface {
	LotQuerier
}
