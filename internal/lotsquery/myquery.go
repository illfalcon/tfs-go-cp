package lotsquery

import (
	"time"

	lotsquerypb "gitlab.com/illfalcon/tfs-go-cp/internal/rpc/lotsquery"

	"github.com/pkg/errors"

	"gitlab.com/illfalcon/tfs-go-cp/internal/lot"
)

type MyQueryService struct {
	LotService lot.Service
}

func lotToLotPB(l lot.Lot) *lotsquerypb.Lot {
	owner := &lotsquerypb.ShortUser{
		ID: int64(l.Creator.ID), FirstName: l.Creator.FirstName, LastName: l.Creator.LastName,
	}
	buyer := &lotsquerypb.ShortUser{
		ID: int64(l.Buyer.ID), FirstName: l.Buyer.FirstName, LastName: l.Buyer.LastName,
	}
	endAtStr := l.EndAt.Format(time.RFC3339)
	lotPB := &lotsquerypb.Lot{
		ID: int64(l.ID), Title: l.Title, Description: l.Description, PriceStep: l.PriceStep,
		MinPrice: l.MinPrice, BuyPrice: l.BuyPrice, Owner: owner, Buyer: buyer, EndAt: endAtStr, Status: l.Status,
	}
	return lotPB
}

func (qs MyQueryService) LotsOfUser(id int, lotType string) ([]*lotsquerypb.Lot, error) {
	ll, err := qs.LotService.LotsOfUser(id, lotType)
	if err != nil {
		return nil, errors.Wrap(err, "error in method lotsquery.LotsOfUser()")
	}
	var respLots []*lotsquerypb.Lot
	for _, l := range ll {
		respLots = append(respLots, lotToLotPB(*l))
	}
	return respLots, nil
}

func (qs MyQueryService) Lot(id int) (*lotsquerypb.Lot, error) {
	l, err := qs.LotService.Lot(id)
	if err != nil {
		return nil, errors.Wrapf(err, "error in lotsquery.Lot(), Lot with id %v", id)
	}
	lotPB := lotToLotPB(*l)
	return lotPB, nil
}

func (qs MyQueryService) Lots(status string) ([]*lotsquerypb.Lot, error) {
	ll, err := qs.LotService.Lots(status)
	if err != nil {
		return nil, errors.Wrap(err, "error in lotsquery.Lots()")
	}
	var respLots []*lotsquerypb.Lot
	for _, l := range ll {
		respLots = append(respLots, lotToLotPB(*l))
	}
	return respLots, nil
}
