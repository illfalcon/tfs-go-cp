package postgres

import (
	"log"

	"github.com/jinzhu/gorm"
	//Blank import needed to register postgres database engine.
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func CreateServices() (UserService, SessionService, LotService, func() error) {
	dsn := "postgres://tinkoff:fintech@localhost:5432/auction" +
		"?sslmode=disable&fallback_application_name=auction-app"
	db, err := gorm.Open("postgres", dsn)
	if err != nil {
		log.Fatalf("can't connect to db: %s", err)
	}
	db.AutoMigrate(&User{}, &Session{}, &Lot{})
	us := UserService{db: db}
	ss := SessionService{db: db}
	ls := LotService{db: db}
	close := db.Close
	return us, ss, ls, close
}
