package postgres

import (
	"time"

	"github.com/pkg/errors"
	"gitlab.com/illfalcon/tfs-go-cp/pkg/weberrors"

	"github.com/jinzhu/gorm"
	"gitlab.com/illfalcon/tfs-go-cp/internal/session"
)

type Session struct {
	SessionID  string `gorm:"primary_key"`
	UserID     int
	CreatedAt  time.Time
	ValidUntil time.Time
	DeletedAt  *time.Time
}

type SessionService struct {
	db *gorm.DB
}

func (ss SessionService) Session(id string) (*session.Session, error) {
	var dbSession Session
	result := ss.db.Where("session_id = ?", id).First(&dbSession)
	if result.RecordNotFound() {
		return nil, errors.Wrapf(weberrors.ErrUnauthorized,
			"session with id %v not found in postgres.Session()", id)
	}
	if result.Error != nil {
		return nil, errors.Wrap(result.Error, "error in postgres.Session()")
	}
	s := session.Session{SessionID: dbSession.SessionID, UserID: dbSession.UserID, CreatedAt: dbSession.CreatedAt,
		ValidUntil: dbSession.ValidUntil}
	return &s, nil
}

func (ss *SessionService) CreateSession(s session.Session) error {
	dbSession := Session{SessionID: s.SessionID, CreatedAt: s.CreatedAt, ValidUntil: s.ValidUntil}
	dbUser, err := queryUserFromDB(ss.db, s.UserID)
	if err != nil {
		return errors.Wrap(err, "error in postgres.CreateSession()")
	}
	dbUser.Sessions = append(dbUser.Sessions, dbSession)
	ss.db.Save(&dbUser)
	return nil
}

//func (ss *SessionService) DeleteSession(id string) error {
//	var dbSession Session
//	result := ss.db.First(&dbSession, id)
//	if result.RecordNotFound() {
//		return errors.Wrapf(weberrors.ErrNotFound,
//			"session with id %v not found in postgres.", id)
//	}
//	if result.Error != nil {
//		return fmt.Errorf("error while querying session with id %v: %v", id, result.Error)
//	}
//	deletion := ss.db.Delete(&dbSession)
//	if deletion.Error != nil {
//		return fmt.Errorf("error while deleting a session with id %v: %v", id, deletion.Error)
//	}
//	return nil
//}
