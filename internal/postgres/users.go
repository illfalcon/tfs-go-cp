package postgres

import (
	"time"

	"gitlab.com/illfalcon/tfs-go-cp/internal/auth"

	"github.com/pkg/errors"

	"gitlab.com/illfalcon/tfs-go-cp/pkg/weberrors"

	"github.com/jinzhu/gorm"
	"gitlab.com/illfalcon/tfs-go-cp/internal/user"
)

type User struct {
	ID         int
	FirstName  string `gorm:"not null"`
	LastName   string `gorm:"not null"`
	EMail      string `gorm:"not null;unique"`
	Sessions   []Session
	Lots       []Lot  `gorm:"foreignkey:CreatorID"`
	BoughtLots []Lot  `gorm:"foreignkey:BuyerID"`
	Password   string `gorm:"not null"`
	Birthday   *time.Time
	CreatedAt  time.Time
	UpdatedAt  time.Time
	DeletedAt  *time.Time
}

type UserService struct {
	db *gorm.DB
}

func dbUserToShortUser(u User) user.ShortUser {
	return user.ShortUser{ID: u.ID, FirstName: u.FirstName, LastName: u.LastName}
}

func queryUserFromDB(db *gorm.DB, id int) (*User, error) {
	var dbUser User
	result := db.First(&dbUser, id)
	if result.RecordNotFound() {
		return nil, errors.Wrapf(weberrors.ErrNotFound,
			"user with id %v not found", id)
	}
	if result.Error != nil {
		return nil, errors.Wrapf(result.Error, "error in method queryUserFromDB() while searching for user %v", id)
	}
	return &dbUser, nil
}

func checkUniqueEmail(db *gorm.DB, email string) (bool, error) {
	var dbUser User
	result := db.Where("e_mail = ?", email).First(&dbUser)
	if result.RecordNotFound() {
		return true, nil
	}
	if result.Error != nil {
		return false, errors.Wrapf(result.Error,
			"error while querying for user with email %v in checkUniqueEmail()", email)
	}
	return false, errors.Wrapf(weberrors.ErrConflict,
		"user with email %v exists", email)
}

func queryUserFromDBByEmail(db *gorm.DB, email string) (*User, error) {
	var dbUser User
	result := db.Where("e_mail = ?", email).First(&dbUser)
	if result.RecordNotFound() {
		return nil, errors.Wrapf(weberrors.ErrUnauthorized,
			"error in queryUserFromDBByID() user with email %v not found", email)
	}
	if result.Error != nil {
		return nil, errors.Wrapf(result.Error,
			"error while querying for user with email %v in queryUserFromDBByEmail()", email)
	}
	return &dbUser, nil
}

func (us UserService) User(id int) (*user.User, error) {
	dbUser, err := queryUserFromDB(us.db, id)
	if err != nil {
		return nil, errors.Wrapf(err, "error in postgres.User(): id = %v", id)
	}
	user := user.User{ID: dbUser.ID, FirstName: dbUser.FirstName, LastName: dbUser.LastName,
		EMail: dbUser.EMail, Birthday: *dbUser.Birthday, CreatedAt: dbUser.CreatedAt}
	return &user, nil
}

func (us UserService) UserByEmailPassword(email, password string) (*user.User, error) {
	dbUser, err := queryUserFromDBByEmail(us.db, email)
	if err != nil {
		return nil, errors.Wrapf(err, "error in UserByEmailPassword()")
	}
	if !auth.CheckPasswordHash(password, dbUser.Password) {
		return nil, errors.Wrapf(weberrors.ErrUnauthorized,
			"wrong password for user with email %v", email)
	}
	user := user.User{ID: dbUser.ID, FirstName: dbUser.FirstName, LastName: dbUser.LastName, EMail: dbUser.EMail, Birthday: *dbUser.Birthday}
	return &user, nil
}

func (us *UserService) CreateUser(u user.User, password string) error {
	tx := us.db.Begin()
	if tx.Error != nil {
		tx.Rollback()
		return errors.Wrap(tx.Error, "error when starting a transaction in postgres.CreateUser()")
	}
	dbUser := User{FirstName: u.FirstName, LastName: u.LastName, EMail: u.EMail, Password: password,
		Birthday: &u.Birthday}
	unique, err := checkUniqueEmail(us.db, u.EMail)
	if err != nil {
		tx.Rollback()
		return errors.Wrap(err, "error in postgres.CreateUser()")
	}
	if !unique {
		tx.Rollback()
		return errors.Wrap(err, "error in postgres.CreateUser()")
	}
	creation := tx.Create(&dbUser)
	if creation.Error != nil {
		tx.Rollback()
		return errors.Wrap(err, "error in postgres.CreateUser()")
	}
	result := tx.Commit()
	if result.Error != nil {
		tx.Rollback()
		return errors.Wrap(err, "error in postgres.CreateUser()")
	}
	return nil
}

func (us *UserService) UpdateUser(id int, u user.User) (*user.User, error) {
	tx := us.db.Begin()
	if tx.Error != nil {
		tx.Rollback()
		return nil, errors.Wrap(tx.Error, "error when starting a transaction in postgres.UpdateUser()")
	}
	var dbUser User
	query := us.db.First(&dbUser, id)
	if query.RecordNotFound() {
		tx.Rollback()
		return nil, errors.Wrapf(weberrors.ErrNotFound,
			"user with id %v not found, err in postgres.UpdateUser()", id)
	}
	if query.Error != nil {
		tx.Rollback()
		return nil, errors.Wrapf(query.Error, "error while querying for user with id %v in postgres.UpdateUser()", id)
	}
	update := tx.Model(&dbUser).Updates(u)
	if update.Error != nil {
		tx.Rollback()
		return nil, errors.Wrapf(update.Error, "error while updating user with id %v in postgres.UpdateUser()", id)
	}
	result := tx.Commit()
	if result.Error != nil {
		tx.Rollback()
		return nil, errors.Wrapf(result.Error, "error while updating a user with id %v in postgres.UpdateUser()", id)
	}
	return &user.User{ID: dbUser.ID, FirstName: dbUser.FirstName, LastName: dbUser.LastName,
		Birthday: *dbUser.Birthday, EMail: dbUser.EMail, CreatedAt: dbUser.CreatedAt}, nil
}

//func (us *UserService) DeleteUser(id int) error {
//	tx := us.db.Begin()
//	if tx.Error != nil {
//		tx.Rollback()
//		return errors.Wrapf(tx.Error, "error when starting a transaction in DeleteUser()")
//	}
//	var dbUser User
//	query := us.db.First(&dbUser, id)
//	if query.RecordNotFound() {
//		tx.Rollback()
//		return errors.Wrapf(weberrors.NotFound{Msg: "error in DeleteUser()"},
//			"user with id %v not found", id)
//	}
//	if query.Error != nil {
//		tx.Rollback()
//		return errors.Wrapf(query.Error, "error while querying for user with id %v in DeleteUser()", id)
//	}
//	deletion := tx.Delete(&dbUser)
//	if deletion.Error != nil {
//		return errors.Wrapf(deletion.Error, "error while deleting a user with id %v in DeleteUser()", id)
//	}
//	result := tx.Commit()
//	if result.Error != nil {
//		tx.Rollback()
//		return errors.Wrapf(result.Error, "error while deleting a user with id %v in DeleteUser()", id)
//	}
//	return nil
//}
