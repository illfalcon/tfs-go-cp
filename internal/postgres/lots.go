package postgres

import (
	"fmt"
	"log"
	"math"
	"time"

	"gitlab.com/illfalcon/tfs-go-cp/pkg/weberrors"

	"github.com/pkg/errors"

	"github.com/jinzhu/gorm"
	"gitlab.com/illfalcon/tfs-go-cp/internal/lot"
)

type Lot struct {
	ID          int
	Creator     User
	CreatorID   int
	Buyer       User
	BuyerID     int
	Title       string
	Description string
	BuyPrice    float64
	MinPrice    float64
	PriceStep   float64 `gorm:"default:1"`
	Status      string  `gorm:"default:'created'"`
	EndAt       time.Time
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   *time.Time
}

func dbLotToLot(l Lot) *lot.Lot {
	return &lot.Lot{ID: l.ID, Creator: dbUserToShortUser(l.Creator),
		Buyer: dbUserToShortUser(l.Buyer), Title: l.Title, Description: l.Description,
		BuyPrice: l.BuyPrice, MinPrice: l.MinPrice, PriceStep: l.PriceStep,
		Status: l.Status, EndAt: l.EndAt}
}

type LotService struct {
	db *gorm.DB
}

func queryLotFromDB(db *gorm.DB, id int) (*Lot, error) {
	var dbLot Lot
	result := db.Preload("Creator").Preload("Buyer").First(&dbLot, id)
	if result.RecordNotFound() {
		return nil, errors.Wrapf(weberrors.ErrNotFound,
			"error while querying for lot with id %v", id)
	}
	if result.Error != nil {
		return nil, fmt.Errorf("error while querying for lot with id %v: %v", id, result.Error)
	}
	return &dbLot, nil
}

func (ls *LotService) CreateLot(lot lot.Lot, creatorID int) (*lot.Lot, error) {
	log.Println(creatorID)
	dbLot := Lot{Title: lot.Title, Description: lot.Description, MinPrice: lot.MinPrice, PriceStep: lot.PriceStep, EndAt: lot.EndAt, CreatorID: creatorID}
	//dbUser, err := queryUserFromDB(ls.db, creatorID)
	//if err != nil {
	//	return nil, errors.Wrap(err, "unable to get user from db in CreateLot()")
	//}
	//dbUser.Lots = append(dbUser.Lots, dbLot)
	ls.db.Save(&dbLot)
	//ls.db.Save(&dbUser)
	retLot, err := queryLotFromDB(ls.db, dbLot.ID)
	if err != nil {
		return nil, errors.Wrap(err, "error in postgres.CreateLot()")
	}
	//log.Printf("%+v\n", retLot)
	return dbLotToLot(*retLot), nil
}

func (ls *LotService) UpdateLot(id int, l lot.Lot) (*lot.Lot, error) {
	dbLot, err := queryLotFromDB(ls.db, id)
	if err != nil {
		return nil, errors.Wrap(err, "error in postgres.UpdateLot()")
	}
	if dbLot.Status != lot.CreatedStatus {
		return nil, errors.Wrap(weberrors.ErrForbidden,
			"error in postgres.UpdateLot(): cannot update active or finished lot")
	}
	tx := ls.db.Begin()
	if tx.Error != nil {
		tx.Rollback()
		return nil, errors.Wrap(tx.Error, "error when starting transaction in postgres.UpdateLot()")
	}
	update := tx.Model(&dbLot).Updates(l)
	if update.Error != nil {
		tx.Rollback()
		return nil, errors.Wrapf(update.Error, "error in postgres.UpdateLot() when updating lot %v", id)
	}
	result := tx.Commit()
	if result.Error != nil {
		tx.Rollback()
		return nil, errors.Wrapf(result.Error, "error while updating l with id %v", id)
	}
	return dbLotToLot(*dbLot), nil
}

func (ls *LotService) DeleteLot(lotID int, deleterID int) error {
	tx := ls.db.Begin()
	if tx.Error != nil {
		tx.Rollback()
		return fmt.Errorf("error when starting a transaction in DeleteLot() method: %v", tx.Error)
	}
	var dbLot Lot
	lotsquery := ls.db.First(&dbLot, lotID)
	if lotsquery.RecordNotFound() {
		tx.Rollback()
		return errors.Wrapf(weberrors.ErrNotFound,
			"lot with id %v not found", lotID)
	}
	if dbLot.CreatorID != deleterID {
		return errors.Wrap(weberrors.ErrForbidden, "error in postgres.DeleteLot()")
	}
	if lotsquery.Error != nil {
		tx.Rollback()
		return fmt.Errorf("error while querying for lot with id %v: %v", lotID, lotsquery.Error)
	}
	deletion := tx.Delete(&dbLot)
	if deletion.Error != nil {
		return fmt.Errorf("error while deleting a lot with id %v: %v", lotID, deletion.Error)
	}
	result := tx.Commit()
	if result.Error != nil {
		tx.Rollback()
		return fmt.Errorf("error while deleting a lot with id %v: %v", lotID, result.Error)
	}
	return nil
}

func (ls LotService) queryLotsOfUserByType(u User, lotType string) ([]*lot.Lot, error) {
	var typesToFKeys = map[string]string{lot.OwnedType: "Lots",
		lot.BoughtType: "BoughtLots"}
	var dbLots []Lot
	query := ls.db.Model(&u).Preload("Creator").Preload("Buyer").Related(&dbLots, typesToFKeys[lotType])
	if query.Error != nil {
		return nil, errors.Wrapf(query.Error, "error when querying for lots of user %v", u.ID)
	}
	var lots []*lot.Lot
	for _, dbLot := range dbLots {
		lots = append(lots, dbLotToLot(dbLot))
	}
	return lots, nil
}

func (ls LotService) queryLotsOfUser(u User) ([]*lot.Lot, error) {
	var dbLots []Lot
	query := ls.db.Model(&u).Preload("Creator").Preload("Buyer").Related(&dbLots, "Lots", "BoughtLots")
	if query.Error != nil {
		return nil, errors.Wrapf(query.Error, "error when querying for lots of user %v", u.ID)
	}
	var lots []*lot.Lot
	for _, dbLot := range dbLots {
		lots = append(lots, dbLotToLot(dbLot))
	}
	return lots, nil
}

func (ls LotService) LotsOfUser(userID int, lotType string) ([]*lot.Lot, error) {
	dbUser, err := queryUserFromDB(ls.db, userID)
	if err != nil {
		return nil, errors.Wrap(err, "error in postgres.LotsOfUser()")
	}
	if lotType == lot.BoughtType || lotType == lot.OwnedType {
		return ls.queryLotsOfUserByType(*dbUser, lotType)
	}
	return ls.queryLotsOfUser(*dbUser)
}

func (ls LotService) Lot(id int) (*lot.Lot, error) {
	dbLot, err := queryLotFromDB(ls.db, id)
	if err != nil {
		return nil, errors.Wrap(err, "error in postgres.Lot()")
	}
	lotObj := dbLotToLot(*dbLot)
	return lotObj, nil
}

func (ls LotService) queryLotsByStatus(status string) ([]*lot.Lot, error) {
	var dbLots []Lot
	var lots []*lot.Lot
	query := ls.db.Where("status LIKE ?", status).Preload("Creator").Preload("Buyer").Find(&dbLots)
	if query.Error != nil {
		return nil, errors.Wrap(query.Error, "error in postgres.Lots()")
	}
	for _, lot := range dbLots {
		lots = append(lots, dbLotToLot(lot))
	}
	return lots, nil
}

func (ls LotService) Lots(status string) ([]*lot.Lot, error) {
	switch status {
	case lot.CreatedStatus:
		return ls.queryLotsByStatus(lot.CreatedStatus)
	case lot.ActiveStatus:
		return ls.queryLotsByStatus(lot.ActiveStatus)
	case lot.FinishedStatus:
		return ls.queryLotsByStatus(lot.FinishedStatus)
	default:
		return ls.queryLotsByStatus(lot.Wildcard)
	}
}

func checkPurchase(l *Lot, buyerID int, price float64) error {
	if l.Status != lot.ActiveStatus {
		return errors.Wrapf(weberrors.ErrForbidden, "lot %v is not active", l.ID)
	}
	if l.BuyerID == buyerID || l.CreatorID == buyerID {
		return errors.Wrapf(weberrors.ErrForbidden, "buyer %v is a current buyer or an owner", buyerID)
	}
	check := (price - l.BuyPrice) / l.PriceStep
	if price < l.BuyPrice || price < l.MinPrice || check != math.Trunc(check) {
		return errors.Wrapf(weberrors.ErrForbidden, "irrelevant price %v", price)
	}
	return nil
}

func (ls *LotService) BuyLot(lotID int, buyerID int, price float64) (*lot.Lot, error) {
	l, err := queryLotFromDB(ls.db, lotID)
	if err != nil {
		return nil, errors.Wrap(err, "error in postgres.BuyLot()")
	}
	err = checkPurchase(l, buyerID, price)
	if err != nil {
		return nil, errors.Wrap(err, "error in postgres.BuyLot()")
	}
	buyer, err := queryUserFromDB(ls.db, buyerID)
	if err != nil {
		return nil, errors.Wrap(err, "error in postgres.BuyLot()")
	}
	l.Buyer = *buyer
	l.BuyPrice = price
	ls.db.Save(&l)
	return dbLotToLot(*l), nil
}

//func (ls *LotService) StartTrade(lotID int) (*lot.Lot, error) {
//	l, err := queryLotFromDB(ls.db, lotID)
//	if err != nil {
//		return nil, errors.Wrap(err, "error in postgres.StartTrade()")
//	}
//	l.Status = lot.ActiveStatus
//	ls.db.Save(&l)
//	return dbLotToLot(*l), nil
//}

func (ls *LotService) EndTrade(l Lot) error {
	l.Status = lot.FinishedStatus
	ls.db.Save(&l)
	return nil
}

func (ls *LotService) EndTrades() ([]*lot.Lot, error) {
	var ll []Lot
	var lots []*lot.Lot
	query := ls.db.Where("end_at < ? AND NOT status = 'finished'", time.Now()).Find(&ll)
	if query.Error != nil {
		return nil, errors.Wrap(query.Error, "error in postgres.EndTrades()")
	}
	for _, l := range ll {
		err := ls.EndTrade(l)
		if err != nil {
			return nil, errors.Wrap(err, "error in postgres.EndTrades()")
		}
		lots = append(lots, dbLotToLot(l))
	}
	return lots, nil
}
