package user

import "time"

type ShortUser struct {
	ID        int    `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}

type User struct {
	ID        int       `json:"id"`
	FirstName string    `json:"first_name"`
	LastName  string    `json:"last_name"`
	EMail     string    `json:"email"`
	Birthday  time.Time `json:"birthday"`
	CreatedAt time.Time `json:"created_at"`
}

type Service interface {
	User(id int) (*User, error)
	UserByEmailPassword(email, password string) (*User, error)
	CreateUser(u User, password string) error
	UpdateUser(id int, u User) (*User, error)
	//DeleteUser(id int) error
}
