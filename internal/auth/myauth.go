package auth

import (
	"crypto/rand"
	"encoding/json"
	"fmt"
	"math/big"
	"time"

	"golang.org/x/crypto/bcrypt"

	"github.com/pkg/errors"
	"gitlab.com/illfalcon/tfs-go-cp/pkg/weberrors"

	"gitlab.com/illfalcon/tfs-go-cp/internal/session"
	"gitlab.com/illfalcon/tfs-go-cp/internal/user"
)

const letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
const sessionDur = 30 * 24 * time.Hour

func randomString(size int) (string, error) {
	token := make([]byte, size)
	for i := range token {
		c, error := rand.Int(rand.Reader, big.NewInt(int64(len(letters))))
		if error != nil {
			return "", fmt.Errorf("error while creating a token: %v", error)
		}
		token[i] = letters[c.Int64()]
	}
	return string(token), nil
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

type MyAuthenticator struct {
	UserService    user.Service
	SessionService session.Service
}

func AuthenticateToken(token string, ss session.Service) (int, error) {
	session, err := ss.Session(token)
	if err != nil {
		return 0, errors.Wrap(err, "error in auth.AuthenticateToken()")
	}
	now := time.Now()
	if now.After(session.ValidUntil) {
		return 0, errors.Wrapf(weberrors.ErrUnauthorized,
			"error in auth.AuthenticateToken(): session %v has expired at %v", token, session.ValidUntil)
	}
	return session.UserID, nil
}

func (a MyAuthenticator) Authenticate(email, password string) (string, error) {
	authUser, err := a.UserService.UserByEmailPassword(email, password)
	if err != nil {
		return "", errors.Wrap(err, "error in auth.Authenticate()")
	}
	token, err := a.authorize(authUser)
	if err != nil {
		return "", errors.Wrap(err, "error in auth.Authenticate()")
	}
	return token, err
}

func (a MyAuthenticator) authenticateToken(token string) (*user.User, error) {
	userID, err := AuthenticateToken(token, a.SessionService)
	if err != nil {
		return nil, errors.Wrap(err, "error in auth.authenticateToken()")
	}
	u, err := a.UserService.User(userID)
	if err != nil {
		return nil, errors.Wrap(err, "error in auth.authenticateToken()")
	}
	return u, nil
}

func (a MyAuthenticator) authorize(user *user.User) (string, error) {
	token, err := randomString(10)
	if err != nil {
		return "", errors.Wrap(err, "error in auth.authorize()")
	}
	now := time.Now()
	session := session.Session{SessionID: token, UserID: user.ID, CreatedAt: now, ValidUntil: now.Add(sessionDur)}
	err = a.SessionService.CreateSession(session)
	if err == weberrors.ErrNotFound {
		return "", errors.Wrap(weberrors.ErrUnauthorized,
			"error in auth.authorize()")
	}
	if err != nil {
		return "", errors.Wrap(err, "error in auth.authorize()")
	}
	return token, nil
}

func (a MyAuthenticator) Register(email, password, firstName, lastName string, birthday time.Time) error {
	u := user.User{EMail: email, FirstName: firstName, LastName: lastName, Birthday: birthday}
	hash, _ := HashPassword(password)
	err := a.UserService.CreateUser(u, string(hash))
	if err != nil {
		return errors.Wrap(err, "error in auth.Register()")
	}
	return nil
}

func (a MyAuthenticator) Update(id int, token, firstName, lastName string, birthday time.Time) ([]byte, error) {
	u, err := a.authenticateToken(token)
	if err != nil {
		return nil, errors.Wrap(err, "auth error in auth.Update()")
	}
	if id != 0 {
		return nil, errors.Wrapf(weberrors.ErrForbidden,
			"unauthorized attempt to update user %v", id)
	}
	newUser := user.User{FirstName: firstName, LastName: lastName, Birthday: birthday}
	user, err := a.UserService.UpdateUser(u.ID, newUser)
	if err != nil {
		return nil, errors.Wrapf(err, "error while updating user %v in auth.Update()", u.ID)
	}
	JSONUser, err := json.Marshal(user)
	if err != nil {
		return nil, errors.Wrap(err, "error in method Update()")
	}
	return JSONUser, nil
}
