package auth

import "time"

type Authenticator interface {
	Authenticate(email, password string) (string, error)
}

type Registrator interface {
	Register(email, password, firstName, lastName string, birthday time.Time) error
}

type Updater interface {
	Update(id int, token, firstName, lastName string, birthday time.Time) ([]byte, error)
}

type Service interface {
	Authenticator
	Registrator
	Updater
}
