package userquery

type UserQuerier interface {
	User(id int, token string) ([]byte, error)
}

type Service interface {
	UserQuerier
}
