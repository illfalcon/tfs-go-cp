package userquery

import (
	"encoding/json"

	"github.com/pkg/errors"
	"gitlab.com/illfalcon/tfs-go-cp/internal/auth"
	"gitlab.com/illfalcon/tfs-go-cp/internal/session"
	"gitlab.com/illfalcon/tfs-go-cp/internal/user"
)

type MyUserQueryService struct {
	UserService    user.Service
	SessionService session.Service
}

func (qs MyUserQueryService) User(id int, token string) ([]byte, error) {
	querier, err := auth.AuthenticateToken(token, qs.SessionService)
	if err != nil {
		return nil, errors.Wrap(err, "error in UserQueryService.User()")
	}
	if id == 0 {
		id = querier
	}
	u, err := qs.UserService.User(id)
	if err != nil {
		return nil, errors.Wrapf(err, "error while querying for u %v in userquery.User(): %v", id)
	}
	userJSON, err := json.Marshal(u)
	if err != nil {
		return nil, errors.Wrapf(err, "error while json encoding u %v in userquery.User(): %v", id)
	}
	return userJSON, nil
}
