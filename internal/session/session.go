package session

import "time"

type Session struct {
	SessionID             string
	UserID                int
	CreatedAt, ValidUntil time.Time
}

type Service interface {
	Session(id string) (*Session, error)
	CreateSession(s Session) error
	//DeleteSession(id string) error
}
