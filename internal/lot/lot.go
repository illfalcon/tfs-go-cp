package lot

import (
	"time"

	"gitlab.com/illfalcon/tfs-go-cp/internal/user"
)

const FinishedStatus string = "finished"
const CreatedStatus string = "created"
const ActiveStatus string = "active"
const Wildcard string = "%"

const OwnedType string = "owned"
const BoughtType string = "bought"

type Lot struct {
	ID          int            `json:"id"`
	Creator     user.ShortUser `json:"creator"`
	Buyer       user.ShortUser `json:"buyer"`
	Title       string         `json:"title"`
	Description string         `json:"description"`
	BuyPrice    float64        `json:"buy_price"`
	MinPrice    float64        `json:"min_price"`
	PriceStep   float64        `json:"price_step"`
	Status      string         `json:"status"`
	EndAt       time.Time      `json:"end_at"`
}

type Service interface {
	CreateLot(lot Lot, creatorID int) (*Lot, error)
	UpdateLot(id int, lot Lot) (*Lot, error)
	DeleteLot(lotID int, deleterID int) error
	LotsOfUser(userID int, lotType string) ([]*Lot, error)
	Lots(status string) ([]*Lot, error)
	Lot(id int) (*Lot, error)
	BuyLot(lotID int, buyerID int, price float64) (*Lot, error)
	EndTrades() ([]*Lot, error)
}
