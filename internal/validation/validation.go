package validation

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"gitlab.com/illfalcon/tfs-go-cp/internal/lot"

	"github.com/pkg/errors"
	"gitlab.com/illfalcon/tfs-go-cp/pkg/weberrors"
)

type SelfValidator interface {
	Validate() error
}

func Decode(r *http.Request, v SelfValidator) error {
	if err := json.NewDecoder(r.Body).Decode(v); err != nil {
		return errors.Wrapf(weberrors.ErrBadRequest, "error in Decode(): %v", err)
	}
	return v.Validate()
}

type NewUser struct {
	FirstName       string `json:"first_name"`
	LastName        string `json:"last_name"`
	EMail, Password string
	Birthday        string
	DecodedBirthday time.Time `json:"-"`
}

type UpdateUser struct {
	FirstName       string `json:"first_name"`
	LastName        string `json:"last_name"`
	Birthday        string
	DecodedBirthday time.Time `json:"-"`
}

type SignInUser struct {
	EMail, Password string
}

func (nu *NewUser) Validate() error {
	if len(nu.EMail) == 0 {
		return errors.Wrap(weberrors.ErrBadRequest, "email missing")
	}
	if len(nu.Password) == 0 {
		return errors.Wrap(weberrors.ErrBadRequest, "password missing")
	}
	if len(nu.FirstName) == 0 {
		return errors.Wrap(weberrors.ErrBadRequest, "first name missing")
	}
	if len(nu.LastName) == 0 {
		return errors.Wrap(weberrors.ErrBadRequest, "last name missing")
	}
	if len(nu.Birthday) != 0 {
		birthday, err := time.Parse("2006-01-02", nu.Birthday)
		if err != nil {
			return errors.Wrapf(weberrors.ErrBadRequest, "malformed birthday %v", nu.Birthday)
		}
		nu.DecodedBirthday = birthday
	}
	return nil
}

func (uu *UpdateUser) Validate() error {
	if len(uu.FirstName) == 0 {
		return errors.Wrap(weberrors.ErrBadRequest, "first name missing")
	}
	if len(uu.LastName) == 0 {
		return errors.Wrap(weberrors.ErrBadRequest, "last name missing")
	}
	if len(uu.Birthday) != 0 {
		birthday, err := time.Parse("2006-01-02", uu.Birthday)
		if err != nil {
			return errors.Wrap(weberrors.ErrBadRequest, "malformed birthday")
		}
		uu.DecodedBirthday = birthday
	}
	return nil
}

func (su SignInUser) Validate() error {
	if len(su.EMail) == 0 {
		return errors.Wrap(weberrors.ErrBadRequest, "email missing")
	}
	if len(su.Password) == 0 {
		return errors.Wrap(weberrors.ErrBadRequest, "password missing")
	}
	return nil
}

type NewLot struct {
	Title        string
	Description  string
	MinPrice     float64   `json:"min_price"`
	PriceStep    float64   `json:"price_step"`
	EndAt        string    `json:"end_at"`
	DecodedEndAt time.Time `json:"-"`
	Status       string
}

func (nl *NewLot) Validate() error {
	if len(nl.Title) == 0 {
		return errors.Wrap(weberrors.ErrBadRequest, "title missing")
	}
	if nl.MinPrice == 0 {
		return errors.Wrap(weberrors.ErrBadRequest, "min price >0 missing")
	}
	if len(nl.EndAt) == 0 {
		return errors.Wrap(weberrors.ErrBadRequest, "end at missing")
	}
	log.Println(nl.EndAt)
	endAt, err := time.Parse("2006-01-02T15:04:05Z07:00", nl.EndAt)
	log.Printf("%v", endAt)
	if err != nil {
		return errors.Wrap(weberrors.ErrBadRequest, "malformed end at")
	}
	nl.DecodedEndAt = endAt
	if nl.Status != lot.CreatedStatus && nl.Status != lot.ActiveStatus && nl.Status != "" {
		return errors.Wrap(weberrors.ErrBadRequest, "unacceptable status")
	}
	log.Println(nl.DecodedEndAt)
	return nil
}

type BuyLot struct {
	Price float64
}

func (bl *BuyLot) Validate() error {
	if bl.Price < 1 {
		return errors.Wrap(weberrors.ErrBadRequest, "unacceptable price!")
	}
	return nil
}
