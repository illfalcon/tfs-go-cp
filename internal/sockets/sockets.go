package sockets

import (
	"fmt"
	"sync"

	"github.com/gorilla/websocket"
)

type WSClients struct {
	WsConn []*websocket.Conn
	sync.Mutex
}

func (clients *WSClients) AddClient(conn *websocket.Conn) {
	clients.Mutex.Lock()
	clients.WsConn = append(clients.WsConn, conn)
	clients.Mutex.Unlock()
	fmt.Printf("added client, total clients: %d\n", len(clients.WsConn))
}

func (clients *WSClients) removeClientByID(id int) {
	clients.Mutex.Lock()
	_ = clients.WsConn[id].Close()
	clients.WsConn = append(clients.WsConn[:id], clients.WsConn[id+1:]...)
	clients.Mutex.Unlock()
}

func (clients *WSClients) BroadcastMessage(message []byte) {
	for i, c := range clients.WsConn {
		err := c.WriteMessage(websocket.TextMessage, message)
		if err != nil {
			fmt.Printf("can't broadcast message: %+v\n", err)
			clients.removeClientByID(i)
		}
	}
}
