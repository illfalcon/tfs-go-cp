package ui

import (
	"html/template"
	"io"
	"net/http"

	"github.com/pkg/errors"
)

func ParseTemplates() map[string]*template.Template {
	var templates map[string]*template.Template
	if templates == nil {
		templates = make(map[string]*template.Template)
	}
	templates["lots"] = template.Must(template.ParseFiles("internal/ui/templates/html/lots.html"))
	templates["lot"] = template.Must(template.ParseFiles("internal/ui/templates/html/lot.html"))
	templates["signin"] = template.Must(template.ParseFiles("internal/ui/templates/html/signin.html"))
	templates["newlot"] = template.Must(template.ParseFiles("internal/ui/templates/html/newlot.html"))
	templates["updatelot"] = template.Must(template.ParseFiles("internal/ui/templates/html/updatelot.html"))
	return templates
}

func RenderTemplate(w io.Writer, tmpl *template.Template, data interface{}) error {
	err := tmpl.Execute(w, data)
	if err != nil {
		return errors.Wrap(err, "error in ui.RenderTemplate()")
	}
	return nil
}

type Renderer interface {
	RenderLots(w http.ResponseWriter, status string, token string) error
	RenderLot(w http.ResponseWriter, id int, token string) error
	RenderSignIn(w http.ResponseWriter) error
	RenderNewLot(w http.ResponseWriter, token string) error
	RenderUpdateLot(w http.ResponseWriter, id int, token string) error
}

type Service interface {
	Renderer
}
