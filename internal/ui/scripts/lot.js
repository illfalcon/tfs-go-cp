function addWS(){
    const socket = new WebSocket('ws://localhost:5000/ws/lots'+document.querySelector("#lot-id").data("id"));
    socket.addEventListener('message', function (event) {
        var msg = JSON.parse(event.data);
        updateLot(msg);
    });
}

function updateLot(lot) {
    const title = document.querySelector('#title');
    const description = document.querySelector('#description');
    const status = document.querySelector('#status');
    const buyer = document.querySelector('#buyer');
    const minPrice = document.querySelector('#min-price');
    const curPrice = document.querySelector('#cur-price');
    const priceStep = document.querySelector('#price-step');
    const endAt = document.querySelector('#end-at');
    title.innerHTML = lot.title;
    description.innerHTML = lot.description;
    buyer.innerHTML = lot.buyer.first_name + " " + lot.buyer.last_name;
    status.innerHTML = lot.status;
    minPrice.innerHTML = lot.min_price;
    curPrice.innerHTML = lot.buy_price;
    priceStep.innerHTML = lot.price_step;
    endAt.innerHTML = lot.end_at;
}

function main() {
    var btn = document.querySelector('#price-submit');
    btn.onclick = function() {
        const request = new XMLHttpRequest();
        const url = btn.data("href");
        request.open("PUT", url);
        request.setRequestHeader("Content-Type", "application/json");
        const authCookie = browser.cookies.get({name: "bearer-token"});
        request.setRequestHeader("Authorization", "Bearer " + authCookie);
        const price = Number(document.querySelector('#price').innerHTML);
        const jsonPrice = JSON.stringify({"price": price});
        request.send(jsonPrice);
    }
    addWS();
}

document.addEventListener('DOMContentLoaded', main);
