function main() {
    var btn = document.querySelector('#signin-button');
    btn.onclick = function() {
        const email = document.querySelector('#inputEmail');
        const password = document.querySelector('#inputPassword');
        const request = new XMLHttpRequest();
        const jsonReq = JSON.stringify({"email": email.value, "password": password.value})
        const url = "https://localhost:5000/api/v1/signin"
        request.open("POST", url)
        request.setRequestHeader("Content-Type", "application/json");
        request.send(jsonReq);
    }
}

document.addEventListener('DOMContentLoaded', main);
