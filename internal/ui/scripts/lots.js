function addWS() {
    const socket = new WebSocket('ws://localhost:5000/ws/lots');
    socket.addEventListener('message', function (event) {
        var msg = JSON.parse(event.data);
        updateLot(msg);
    });
}

function updateLot(lot) {
    const title = document.querySelector('#title-'+lot.ID);
    const description = document.querySelector('#description-'+lot.ID);
    const status = document.querySelector('#status-'+lot.ID);
    const priceStep = document.querySelector('#price-step-'+lot.ID);
    const buyPrice = document.querySelector('#buy-price-'+lot.ID);
    const endAt = document.querySelector('#end-at-'+lot.ID);
    title.innerHTML = lot.Title;
    description.innerHTML = lot.Description;
    status.innerHTML = lot.Status;
    priceStep.innerHTML = lot.PriceStep;
    buyPrice.innerHTML = lot.BuyPrice;
    endAt.innerHTML = lot.EndAt;
}

function main() {
    var lots = document.querySelectorAll('.clickable-row');
    for (var lot of lots) {
        lot.onclick = function() {
            window.location.href = lot.data('href');
        }
    }
    addWS();
}

document.addEventListener('DOMContentLoaded', main);
