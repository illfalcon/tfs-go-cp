package ui

import (
	"encoding/json"
	"html/template"
	"net/http"

	"gitlab.com/illfalcon/tfs-go-cp/internal/lotsgateway"

	"github.com/pkg/errors"
	"gitlab.com/illfalcon/tfs-go-cp/internal/auth"
	"gitlab.com/illfalcon/tfs-go-cp/internal/lot"
	"gitlab.com/illfalcon/tfs-go-cp/internal/session"
	"gitlab.com/illfalcon/tfs-go-cp/pkg/weberrors"
)

type MyUIService struct {
	Templates      map[string]*template.Template
	SessionService session.Service
	LotService     lotsgateway.Service
}

func (mus MyUIService) RenderLots(w http.ResponseWriter, status string, token string) error {
	llJSON, err := mus.LotService.Lots(token, status)
	if err != nil {
		return errors.Wrap(err, "error in ui.RenderLots()")
	}
	var lotsModel struct {
		Lots []*lot.Lot
	}
	var ll []*lot.Lot
	_ = json.Unmarshal(llJSON, &ll)
	lotsModel.Lots = ll
	tmpl, ok := mus.Templates["lots"]
	if !ok {
		return errors.Wrap(weberrors.ErrNotFound, "template 'lots' not found")
	}
	err = RenderTemplate(w, tmpl, lotsModel)
	if err != nil {
		return errors.Wrap(err, "error in ui.RenderLots()")
	}
	return nil
}

func (mus MyUIService) RenderLot(w http.ResponseWriter, id int, token string) error {
	JSONl, err := mus.LotService.Lot(id, token)
	if err != nil {
		return errors.Wrap(err, "error in ui.Lot()")
	}
	tmpl, ok := mus.Templates["lot"]
	if !ok {
		return errors.Wrap(weberrors.ErrNotFound, "template 'lot' not found")
	}
	var l *lot.Lot
	_ = json.Unmarshal(JSONl, &l)
	err = RenderTemplate(w, tmpl, l)
	if err != nil {
		return errors.Wrap(err, "error in ui.RenderLot()")
	}
	return nil
}

func (mus MyUIService) RenderSignIn(w http.ResponseWriter) error {
	tmpl, ok := mus.Templates["signin"]
	if !ok {
		return errors.Wrap(weberrors.ErrNotFound, "template 'signin' not found")
	}
	err := RenderTemplate(w, tmpl, nil)
	if err != nil {
		return errors.Wrap(err, "error in ui.RenderSignIn()")
	}
	return nil
}

func (mus MyUIService) RenderNewLot(w http.ResponseWriter, token string) error {
	_, err := auth.AuthenticateToken(token, mus.SessionService)
	if err != nil {
		return errors.Wrap(err, "error in ui.RenderLots()")
	}
	tmpl, ok := mus.Templates["newlot"]
	if !ok {
		return errors.Wrap(weberrors.ErrNotFound, "template 'newlot' not found")
	}
	err = RenderTemplate(w, tmpl, nil)
	if err != nil {
		return errors.Wrap(err, "error in ui.RenderNewLot()")
	}
	return nil
}

func (mus MyUIService) RenderUpdateLot(w http.ResponseWriter, id int, token string) error {
	l, err := mus.LotService.Lot(id, token)
	if err != nil {
		return errors.Wrap(err, "error in ui.RenderUpdateLot()")
	}
	tmpl, ok := mus.Templates["updatelot"]
	if !ok {
		return errors.Wrap(weberrors.ErrNotFound, "template 'updatelot' not found")
	}
	err = RenderTemplate(w, tmpl, l)
	if err != nil {
		return errors.Wrap(err, "error in ui.RenderUpdateLot()")
	}
	return nil
}
