//Deprecated package for in-memory data storage. Served for early development.
package fakedb

//
//import (
//	"fmt"
//	"time"
//
//	"gitlab.com/illfalcon/tfs-go-cp/internal/session"
//	"gitlab.com/illfalcon/tfs-go-cp/internal/user"
//)
//
////fakeDBUser is mocking the internal implementation of User object in a database or any other storage.
//type fakeDBUser struct {
//	ID                   int
//	FirstName, LastName  string
//	EMail, Password      string
//	CreatedAt, UpdatedAt time.Time
//}
//
//type UserService struct {
//	globalUserID int
//	userList     []*fakeDBUser
//}
//
//func (us UserService) findUserByEmailAndPassword(email, password string) (*fakeDBUser, error) {
//	for _, u := range us.userList {
//		if u.EMail == email && u.Password == password {
//			return u, nil
//		}
//	}
//	return nil, fmt.Errorf("user with email: %s and password: %s not found", email, password)
//}
//
//func (us UserService) findUserByID(id int) (int, *fakeDBUser, error) {
//	for i, u := range us.userList {
//		if u.ID == id {
//			return i, u, nil
//		}
//	}
//	return -1, nil, fmt.Errorf("user with id: %v not found", id)
//}
//
//func (us UserService) checkUniqueEmail(email string) bool {
//	for _, u := range us.userList {
//		if u.EMail == email {
//			return false
//		}
//	}
//	return true
//}
//
//func (us UserService) User(id int) (*user.User, error) {
//	_, dbUser, err := us.findUserByID(id)
//	if err == nil {
//		u := user.User{ID: dbUser.ID, FirstName: dbUser.FirstName, LastName: dbUser.LastName, CreatedAt: dbUser.CreatedAt, UpdatedAt: dbUser.UpdatedAt}
//		return &u, nil
//	}
//	return nil, err
//}
//
//func (us UserService) UserByEmailPassword(email, password string) (*user.User, error) {
//	dbUser, err := us.findUserByEmailAndPassword(email, password)
//	if err == nil {
//		u := user.User{ID: dbUser.ID, FirstName: dbUser.FirstName, LastName: dbUser.LastName, CreatedAt: dbUser.CreatedAt, UpdatedAt: dbUser.UpdatedAt}
//		return &u, nil
//	}
//	return nil, err
//}
//
//func (us *UserService) CreateUser(u *user.User) error {
//	if !us.checkUniqueEmail(u.EMail) {
//		return fmt.Errorf("a user with such email already exists")
//	}
//	us.globalUserID++
//	now := time.Now()
//	u.ID = us.globalUserID
//	dbUser := fakeDBUser{ID: us.globalUserID, EMail: u.EMail, Password: u.Password, CreatedAt: now, UpdatedAt: now}
//	us.userList = append(us.userList, &dbUser)
//	return nil
//}
//
//func (us *UserService) UpdateUser(id int, u *user.User) error {
//	_, dbUser, err := us.findUserByID(id)
//	if err != nil {
//		return err
//	}
//	dbUser.FirstName = u.FirstName
//	dbUser.LastName = u.LastName
//	dbUser.UpdatedAt = time.Now()
//	return nil
//}
//
//func (us *UserService) DeleteUser(id int) error {
//	i, _, err := us.findUserByID(id)
//	if err != nil {
//		return err
//	}
//	var newList []*fakeDBUser
//	newList = append(newList, us.userList[:i]...)
//	newList = append(newList, us.userList[i+1:]...)
//	us.userList = newList
//	return nil
//}
//
//type fakeDBSession struct {
//	SessionID             string
//	UserID                int
//	CreatedAt, ValidUntil time.Time
//}
//
//type SessionService struct {
//	sessionList []*fakeDBSession
//}
//
//func (ss SessionService) findSessionByID(id string) (int, *fakeDBSession, error) {
//	for i, s := range ss.sessionList {
//		if s.SessionID == id {
//			return i, s, nil
//		}
//	}
//	return -1, nil, fmt.Errorf("session with id: %v not found", id)
//}
//
//func (ss SessionService) Session(id string) (*session.Session, error) {
//	_, dbSes, err := ss.findSessionByID(id)
//	if err != nil {
//		return nil, fmt.Errorf("error while querying for session %v: %v", id, err)
//	}
//	s := session.Session{SessionID: dbSes.SessionID, UserID: dbSes.UserID, CreatedAt: dbSes.CreatedAt, ValidUntil: dbSes.ValidUntil}
//	return &s, nil
//}
//
//func (ss *SessionService) CreateSession(s *session.Session) error {
//	dbSes := fakeDBSession{SessionID: s.SessionID, UserID: s.UserID, CreatedAt: s.CreatedAt, ValidUntil: s.ValidUntil}
//	ss.sessionList = append(ss.sessionList, &dbSes)
//	return nil
//}
//
//func (ss *SessionService) DeleteSession(id string) error {
//	i, _, err := ss.findSessionByID(id)
//	if err != nil {
//		return fmt.Errorf("error while deleting session: %v", err)
//	}
//	var newList []*fakeDBSession
//	newList = append(newList, ss.sessionList[:i]...)
//	newList = append(newList, ss.sessionList[i+1:]...)
//	ss.sessionList = newList
//	return nil
//}
