package main

//
//import (
//	"context"
//	"encoding/json"
//	"fmt"
//	"log"
//	"net/http"
//	"os"
//	"os/signal"
//	"strconv"
//	"strings"
//	"syscall"
//	"time"
//
//	"github.com/gorilla/websocket"
//
//	"gitlab.com/illfalcon/tfs-go-cp/internal/sockets"
//
//	"gitlab.com/illfalcon/tfs-go-cp/internal/ui"
//
//	"gitlab.com/illfalcon/tfs-go-cp/internal/validation"
//
//	"github.com/pkg/errors"
//	"gitlab.com/illfalcon/tfs-go-cp/pkg/weberrors"
//
//	"gitlab.com/illfalcon/tfs-go-cp/internal/interactions"
//
//	"github.com/go-chi/chi"
//	"gitlab.com/illfalcon/tfs-go-cp/internal/auth"
//	"gitlab.com/illfalcon/tfs-go-cp/internal/lotsquery"
//)
//
//type AuthHandler struct {
//	AuthService auth.Service
//}
//
//func (ah AuthHandler) SignUp(w http.ResponseWriter, r *http.Request) {
//	var u validation.NewUser
//	err := validation.Decode(r, &u)
//	if err != nil {
//		log.Println(fmt.Sprintf("error while parsing request: %v", err))
//		switch errors.Cause(err) {
//		case weberrors.ErrBadRequest:
//			http.Error(w, err.Error(), http.StatusBadRequest)
//			return
//		default:
//			http.Error(w, err.Error(), http.StatusInternalServerError)
//			return
//		}
//	}
//	err = ah.AuthService.Register(u.EMail, u.Password, u.FirstName, u.LastName, u.DecodedBirthday)
//	if err != nil {
//		switch errors.Cause(err).(type) {
//		case weberrors.NotFound:
//			http.Error(w, "User not found", http.StatusNotFound)
//		case weberrors.Conflict:
//			http.Error(w, errors.Cause(err).Error(), http.StatusConflict)
//		default:
//			http.Error(w, err.Error(), http.StatusInternalServerError)
//		}
//		log.Println(fmt.Sprintf("error while handling registration: %v", err))
//		return
//	}
//	w.WriteHeader(http.StatusCreated)
//	_, _ = w.Write([]byte("user registered"))
//}
//
//func (ah AuthHandler) SignIn(w http.ResponseWriter, r *http.Request) {
//	log.Println("request")
//	var u validation.SignInUser
//	err := validation.Decode(r, &u)
//	if err != nil {
//		log.Println(fmt.Sprintf("error while parsing request: %v", err))
//		switch errors.Cause(err) {
//		case weberrors.ErrBadRequest:
//			http.Error(w, err.Error(), http.StatusBadRequest)
//			return
//		default:
//			http.Error(w, err.Error(), http.StatusInternalServerError)
//			return
//		}
//	}
//	bearer, err := ah.AuthService.Authenticate(u.EMail, u.Password)
//	if err != nil {
//		switch errors.Cause(err).(type) {
//		case weberrors.Unauthorized:
//			http.Error(w, "Unauthorized access", http.StatusUnauthorized)
//		case weberrors.NotFound:
//			http.Error(w, "User not found", http.StatusNotFound)
//		default:
//			http.Error(w, err.Error(), http.StatusInternalServerError)
//		}
//		log.Println(fmt.Sprintf("error while handling authentication: %v", err))
//		return
//	}
//	resp := struct {
//		TokenType    string `json:"token_type"`
//		AccessString string `json:"access_string"`
//	}{TokenType: "bearer", AccessString: bearer}
//	jsonResp, err := json.Marshal(resp)
//	if err != nil {
//		http.Error(w, err.Error(), http.StatusInternalServerError)
//		return
//	}
//	cookie := http.Cookie{Name: "bearer-token", Value: bearer, Expires: time.Now().Add(30 * 24 * time.Hour), Path: "/"}
//	http.SetCookie(w, &cookie)
//	w.WriteHeader(http.StatusOK)
//	_, _ = w.Write(jsonResp)
//
//}
//
//func switchErrorTypes(w http.ResponseWriter, err error) {
//	switch errors.Cause(err).(type) {
//	case weberrors.Unauthorized:
//		http.Error(w, "Unauthorized access", http.StatusUnauthorized)
//	case weberrors.NotFound:
//		http.Error(w, "User not found", http.StatusNotFound)
//	case weberrors.Forbidden:
//		http.Error(w, "You are not allowed to do this", http.StatusForbidden)
//	default:
//		http.Error(w, err.Error(), http.StatusInternalServerError)
//	}
//}
//
//func (ah AuthHandler) UpdateUser(w http.ResponseWriter, r *http.Request) {
//	bearer := r.Header.Get("Authorization")
//	userIDStr := chi.URLParam(r, "id")
//	userID, err := strconv.Atoi(userIDStr)
//	if err != nil {
//		log.Println(fmt.Sprintf("error while parsing id: %v", userIDStr))
//		http.Error(w, err.Error(), http.StatusBadRequest)
//		return
//	}
//	if bearer == "" {
//		log.Println(fmt.Sprintf("bearer token for user %v not provided", userID))
//		http.Error(w, fmt.Sprintf("bearer token for user %v not provided", userID), http.StatusUnauthorized)
//		return
//	}
//	token := bearer[strings.Index(bearer, " ")+1:]
//	var u validation.UpdateUser
//	err = validation.Decode(r, &u)
//	if err != nil {
//		log.Println(fmt.Sprintf("error while parsing request: %v", err))
//		switch errors.Cause(err) {
//		case weberrors.ErrBadRequest:
//			http.Error(w, err.Error(), http.StatusBadRequest)
//			return
//		default:
//			http.Error(w, err.Error(), http.StatusInternalServerError)
//			return
//		}
//	}
//	jsonUser, err := ah.AuthService.Update(userID, token, u.FirstName, u.LastName, u.DecodedBirthday)
//	if err != nil {
//		switchErrorTypes(w, err)
//		return
//	}
//	w.WriteHeader(http.StatusOK)
//	_, _ = w.Write(jsonUser)
//}
//
//type QueryHandler struct {
//	UserQueryService lotsquery.Service
//}
//
//func (qh QueryHandler) user(w http.ResponseWriter, r *http.Request) []byte {
//	bearer := r.Header.Get("Authorization")
//	userIDStr := chi.URLParam(r, "id")
//	userID, err := strconv.Atoi(userIDStr)
//	if err != nil {
//		log.Println(fmt.Sprintf("error while parsing id: %v", userIDStr))
//		http.Error(w, err.Error(), http.StatusBadRequest)
//		return nil
//	}
//	if bearer == "" {
//		log.Println(fmt.Sprintf("bearer token for user %v not provided", userID))
//		http.Error(w, fmt.Sprintf("bearer token for user %v not provided", userID), http.StatusBadRequest)
//		return nil
//	}
//	token := bearer[strings.Index(bearer, " ")+1:]
//	user, err := qh.UserQueryService.User(userID, token)
//	if err != nil {
//		switch errors.Cause(err).(type) {
//		case weberrors.NotFound:
//			http.Error(w, "User not found", http.StatusNotFound)
//		case weberrors.Unauthorized:
//			http.Error(w, "unauthorized access", http.StatusUnauthorized)
//		default:
//			http.Error(w, err.Error(), http.StatusInternalServerError)
//		}
//		log.Println(fmt.Sprintf("error while querying for user %v: %v", userID, err))
//		return nil
//	}
//	return user
//}
//
//func (qh QueryHandler) User(w http.ResponseWriter, r *http.Request) {
//	user := qh.user(w, r)
//	if user != nil {
//		w.WriteHeader(http.StatusOK)
//		_, _ = w.Write(user)
//	}
//}
//
//func (qh QueryHandler) Lots(w http.ResponseWriter, r *http.Request) {
//	bearer := r.Header.Get("Authorization")
//	if bearer == "" {
//		log.Println(fmt.Sprint("bearer token not provided"))
//		http.Error(w, fmt.Sprint("bearer token not provided"), http.StatusUnauthorized)
//		return
//	}
//	token := bearer[strings.Index(bearer, " ")+1:]
//	statuses, ok := r.URL.Query()["status"]
//	status := ""
//	if ok {
//		status = statuses[0]
//	}
//	lots, err := qh.UserQueryService.Lots(token, status)
//	if err != nil {
//		switch errors.Cause(err).(type) {
//		case weberrors.Unauthorized:
//			http.Error(w, "unauthorized access", http.StatusUnauthorized)
//		default:
//			http.Error(w, err.Error(), http.StatusInternalServerError)
//		}
//		log.Println(fmt.Sprint(err))
//		return
//	}
//	w.WriteHeader(http.StatusOK)
//	_, _ = w.Write(lots)
//}
//
//func (qh QueryHandler) LotsOfUser(w http.ResponseWriter, r *http.Request) {
//	bearer := r.Header.Get("Authorization")
//	userIDStr := chi.URLParam(r, "id")
//	userID, err := strconv.Atoi(userIDStr)
//	if err != nil {
//		log.Println(fmt.Sprintf("error while parsing id: %v", userIDStr))
//		http.Error(w, err.Error(), http.StatusBadRequest)
//		return
//	}
//	if bearer == "" {
//		log.Println(fmt.Sprintf("bearer token for user %v not provided", userID))
//		http.Error(w, fmt.Sprint("bearer token not provided"), http.StatusUnauthorized)
//		return
//	}
//	token := bearer[strings.Index(bearer, " ")+1:]
//	types, ok := r.URL.Query()["type"]
//	lotType := ""
//	if ok {
//		lotType = types[0]
//	}
//	lots, err := qh.UserQueryService.LotsOfUser(userID, token, lotType)
//	if err != nil {
//		switch errors.Cause(err).(type) {
//		case weberrors.Unauthorized:
//			http.Error(w, "unauthorized access", http.StatusUnauthorized)
//		case weberrors.NotFound:
//			http.Error(w, "resource not found", http.StatusNotFound)
//		default:
//			http.Error(w, err.Error(), http.StatusInternalServerError)
//		}
//		log.Println(fmt.Sprint(err))
//		return
//	}
//	w.WriteHeader(http.StatusOK)
//	_, _ = w.Write(lots)
//}
//
//func (qh QueryHandler) Lot(w http.ResponseWriter, r *http.Request) {
//	bearer := r.Header.Get("Authorization")
//	lotIDStr := chi.URLParam(r, "id")
//	lotID, err := strconv.Atoi(lotIDStr)
//	if err != nil {
//		log.Println(fmt.Sprintf("error while parsing id: %v", lotIDStr))
//		http.Error(w, err.Error(), http.StatusBadRequest)
//		return
//	}
//	if bearer == "" {
//		log.Println(fmt.Sprint("bearer token for user not provided"))
//		http.Error(w, fmt.Sprint("bearer token not provided"), http.StatusUnauthorized)
//		return
//	}
//	token := bearer[strings.Index(bearer, " ")+1:]
//	lot, err := qh.UserQueryService.Lot(lotID, token)
//	if err != nil {
//		switchErrorTypes(w, err)
//		return
//	}
//	w.WriteHeader(http.StatusOK)
//	_, _ = w.Write(lot)
//}
//
//type InteractionsHandler struct {
//	InterService  interactions.Service
//	WSLotClients  *sockets.WSClients
//	WSLotsClients *sockets.WSClients
//}
//
//func (ih InteractionsHandler) NewLot(w http.ResponseWriter, r *http.Request) {
//	bearer := r.Header.Get("Authorization")
//	token := bearer[strings.Index(bearer, " ")+1:]
//	var l validation.NewLot
//	err := validation.Decode(r, &l)
//	if err != nil {
//		log.Println(fmt.Sprintf("error while parsing request: %v", err))
//		switch errors.Cause(err) {
//		case weberrors.ErrBadRequest:
//			http.Error(w, err.Error(), http.StatusBadRequest)
//			return
//		default:
//			http.Error(w, err.Error(), http.StatusInternalServerError)
//			return
//		}
//	}
//	err = ih.InterService.NewLot(l.Title, l.Description, l.MinPrice, l.PriceStep, l.DecodedEndAt, token)
//	if err != nil {
//		switch errors.Cause(err).(type) {
//		case weberrors.NotFound:
//			http.Error(w, "Lot not found", http.StatusNotFound)
//		case weberrors.Unauthorized:
//			http.Error(w, "Unauthorized access", http.StatusUnauthorized)
//		default:
//			http.Error(w, err.Error(), http.StatusInternalServerError)
//		}
//		log.Println(fmt.Sprintf("error while handling lot creation: %v", err))
//		return
//	}
//	w.WriteHeader(http.StatusCreated)
//	_, _ = w.Write([]byte("lot created"))
//}
//
//func (ih InteractionsHandler) UpdateLot(w http.ResponseWriter, r *http.Request) {
//	bearer := r.Header.Get("Authorization")
//	lotIDStr := chi.URLParam(r, "id")
//	lotID, err := strconv.Atoi(lotIDStr)
//	if err != nil {
//		log.Println(fmt.Sprintf("error while parsing lot id: %v", lotIDStr))
//		http.Error(w, err.Error(), http.StatusBadRequest)
//		return
//	}
//	if bearer == "" {
//		log.Println("bearer token not provided")
//		http.Error(w, "bearer token for user %v not provided", http.StatusBadRequest)
//		return
//	}
//	token := bearer[strings.Index(bearer, " ")+1:]
//	var l validation.NewLot
//	err = validation.Decode(r, &l)
//	if err != nil {
//		switch errors.Cause(err) {
//		case weberrors.ErrBadRequest:
//			http.Error(w, err.Error(), http.StatusBadRequest)
//			return
//		default:
//			http.Error(w, err.Error(), http.StatusInternalServerError)
//			return
//		}
//	}
//	lot, err := ih.InterService.UpdateLot(lotID, l.Title, l.Description, l.MinPrice, l.PriceStep, l.Status, l.DecodedEndAt, token)
//	if err != nil {
//		switch errors.Cause(err).(type) {
//		case weberrors.NotFound:
//			http.Error(w, "Lot not found", http.StatusNotFound)
//		case weberrors.Unauthorized:
//			http.Error(w, "Unauthorized access", http.StatusUnauthorized)
//		default:
//			http.Error(w, err.Error(), http.StatusInternalServerError)
//		}
//		log.Println(fmt.Sprintf("error while handling lot updating: %v", err))
//		return
//	}
//	w.WriteHeader(http.StatusOK)
//	_, _ = w.Write(lot)
//	ih.WSLotsClients.BroadcastMessage(lot)
//	ih.WSLotClients.BroadcastMessage(lot)
//}
//
//func (ih InteractionsHandler) BuyLot(w http.ResponseWriter, r *http.Request) {
//	bearer := r.Header.Get("Authorization")
//	lotIDStr := chi.URLParam(r, "id")
//	lotID, err := strconv.Atoi(lotIDStr)
//	if err != nil {
//		log.Println(fmt.Sprintf("error while parsing lot id: %v", lotIDStr))
//		http.Error(w, err.Error(), http.StatusBadRequest)
//		return
//	}
//	if bearer == "" {
//		log.Println("bearer token not provided")
//		http.Error(w, "bearer token for user %v not provided", http.StatusBadRequest)
//		return
//	}
//	token := bearer[strings.Index(bearer, " ")+1:]
//	var buyLot validation.BuyLot
//	err = validation.Decode(r, &buyLot)
//	if err != nil {
//		switch errors.Cause(err) {
//		case weberrors.ErrBadRequest:
//			http.Error(w, err.Error(), http.StatusBadRequest)
//			return
//		default:
//			http.Error(w, err.Error(), http.StatusInternalServerError)
//			return
//		}
//	}
//	l, err := ih.InterService.BuyLot(lotID, buyLot.Price, token)
//	if err != nil {
//		log.Println(err)
//		switchErrorTypes(w, err)
//	}
//	w.WriteHeader(http.StatusOK)
//	_, _ = w.Write(l)
//	ih.WSLotsClients.BroadcastMessage(l)
//	ih.WSLotClients.BroadcastMessage(l)
//}
//
////func (ih InteractionsHandler) DeleteLot(w http.ResponseWriter, r *http.Request) {
////	bearer := r.Header.Get("Authorization")
////	lotIDStr := chi.URLParam(r, "id")
////	lotID, err := strconv.Atoi(lotIDStr)
////	if err != nil {
////		log.Println(fmt.Sprintf("error while parsing lot id: %v", lotIDStr))
////		http.Error(w, err.Error(), http.StatusBadRequest)
////		return
////	}
////	if bearer == "" {
////		log.Println("bearer token not provided")
////		http.Error(w, "bearer token for user %v not provided", http.StatusBadRequest)
////		return
////	}
////	token := bearer[strings.Index(bearer, " ")+1:]
////	err = ih.InterService.DeleteLot(lotID, token)
////	if err != nil {
////		switch errors.Cause(err).(type) {
////		case weberrors.NotFound:
////			http.Error(w, "Lot not found", http.StatusNotFound)
////		case weberrors.Unauthorized:
////			http.Error(w, "Unauthorized access", http.StatusUnauthorized)
////		default:
////			http.Error(w, err.Error(), http.StatusInternalServerError)
////		}
////		log.Println(fmt.Sprintf("error while handling lot deletion: %v", err))
////		return
////	}
////	w.WriteHeader(http.StatusOK)
////	_, _ = w.Write([]byte("lot deleted"))
////}
//
//type UIHandler struct {
//	UIService ui.Service
//}
//
//func (ui UIHandler) Lots(w http.ResponseWriter, r *http.Request) {
//	cookie, err := r.Cookie("bearer-token")
//	if err != nil {
//		log.Println(fmt.Sprint("bearer token not provided"))
//		http.Error(w, fmt.Sprint("bearer token not provided"), http.StatusUnauthorized)
//		return
//	}
//	bearer := cookie.Value
//	if bearer == "" {
//		log.Println(fmt.Sprint("bearer token not provided"))
//		http.Error(w, fmt.Sprint("bearer token not provided"), http.StatusUnauthorized)
//		return
//	}
//	statuses, ok := r.URL.Query()["status"]
//	status := ""
//	if ok {
//		status = statuses[0]
//	}
//	err = ui.UIService.RenderLots(w, status, bearer)
//	if err != nil {
//		switchErrorTypes(w, err)
//	}
//}
//
//func (ui UIHandler) Lot(w http.ResponseWriter, r *http.Request) {
//	cookie, err := r.Cookie("bearer-token")
//	if err != nil {
//		log.Println(fmt.Sprint("bearer token not provided"))
//		http.Error(w, fmt.Sprint("bearer token not provided"), http.StatusUnauthorized)
//		return
//	}
//	bearer := cookie.Value
//	if bearer == "" {
//		log.Println(fmt.Sprint("bearer token not provided"))
//		http.Error(w, fmt.Sprint("bearer token not provided"), http.StatusUnauthorized)
//		return
//	}
//	lotIDStr := chi.URLParam(r, "id")
//	lotID, err := strconv.Atoi(lotIDStr)
//	if err != nil {
//		log.Println(fmt.Sprintf("error while parsing id: %v", lotIDStr))
//		http.Error(w, err.Error(), http.StatusBadRequest)
//		return
//	}
//	err = ui.UIService.RenderLot(w, lotID, bearer)
//	if err != nil {
//		switchErrorTypes(w, err)
//	}
//}
//
//func (ui UIHandler) SignIn(w http.ResponseWriter, r *http.Request) {
//	err := ui.UIService.RenderSignIn(w)
//	if err != nil {
//		switchErrorTypes(w, err)
//	}
//}
//
//func (ui UIHandler) NewLot(w http.ResponseWriter, r *http.Request) {
//	cookie, err := r.Cookie("bearer-token")
//	if err != nil {
//		log.Println(fmt.Sprint("bearer token not provided"))
//		http.Error(w, fmt.Sprint("bearer token not provided"), http.StatusUnauthorized)
//		return
//	}
//	bearer := cookie.Value
//	if bearer == "" {
//		log.Println(fmt.Sprint("bearer token not provided"))
//		http.Error(w, fmt.Sprint("bearer token not provided"), http.StatusUnauthorized)
//		return
//	}
//	err = ui.UIService.RenderNewLot(w, bearer)
//	if err != nil {
//		switchErrorTypes(w, err)
//	}
//}
//
//func (ui UIHandler) UpdateLot(w http.ResponseWriter, r *http.Request) {
//	cookie, err := r.Cookie("bearer-token")
//	if err != nil {
//		log.Println(fmt.Sprint("bearer token not provided"))
//		http.Error(w, fmt.Sprint("bearer token not provided"), http.StatusUnauthorized)
//		return
//	}
//	bearer := cookie.Value
//	if bearer == "" {
//		log.Println(fmt.Sprint("bearer token not provided"))
//		http.Error(w, fmt.Sprint("bearer token not provided"), http.StatusUnauthorized)
//		return
//	}
//	lotIDStr := chi.URLParam(r, "id")
//	lotID, err := strconv.Atoi(lotIDStr)
//	if err != nil {
//		log.Println(fmt.Sprintf("error while parsing id: %v", lotIDStr))
//		http.Error(w, err.Error(), http.StatusBadRequest)
//		return
//	}
//	err = ui.UIService.RenderUpdateLot(w, lotID, bearer)
//	if err != nil {
//		switchErrorTypes(w, err)
//	}
//}
//
//type WSHandler struct {
//	Upgrader      websocket.Upgrader
//	WSLotsClients *sockets.WSClients
//	WSLotClients  *sockets.WSClients
//}
//
//func (ws *WSHandler) WSLotsUpdate(w http.ResponseWriter, r *http.Request) {
//	conn, err := ws.Upgrader.Upgrade(w, r, nil)
//	if err != nil {
//		fmt.Printf("can't upgrade connection: %s\n", err)
//		return
//	}
//	ws.WSLotsClients.AddClient(conn)
//}
//
//func (ws *WSHandler) WSLotUpdate(w http.ResponseWriter, r *http.Request) {
//	conn, err := ws.Upgrader.Upgrade(w, r, nil)
//	if err != nil {
//		fmt.Printf("can't upgrade connection: %s\n", err)
//		return
//	}
//	ws.WSLotClients.AddClient(conn)
//}
//
//type Handlers struct {
//	Auth  AuthHandler
//	Query QueryHandler
//	Inter InteractionsHandler
//	UI    UIHandler
//	WS    *WSHandler
//}
//
//func (h Handlers) Start(closeDB func() error) {
//	r := chi.NewRouter()
//	r.Route("/api/v1/", func(r chi.Router) {
//		r.Post("/signup", h.Auth.SignUp)
//		r.Post("/signin", h.Auth.SignIn)
//		r.Put("/users/{id}", h.Auth.UpdateUser)
//		r.Get("/users/{id}", h.Query.User)
//		r.Post("/lots", h.Inter.NewLot)
//		r.Put("/lots/{id}", h.Inter.UpdateLot)
//		r.Get("/lots/{id}", h.Query.Lot)
//		//r.Delete("/lots/{id}", h.Inter.DeleteLot)
//		r.Get("/users/{id}/lots", h.Query.LotsOfUser)
//		r.Get("/lots", h.Query.Lots)
//		r.Put("/lots/{id}/buy", h.Inter.BuyLot)
//	})
//	r.Route("/", func(r chi.Router) {
//		r.Get("/lots", h.UI.Lots)
//		r.Get("/lots/{id}", h.UI.Lot)
//		r.Get("/signin", h.UI.SignIn)
//		r.Get("/lots/new", h.UI.NewLot)
//		r.Get("/lots/{id}/update", h.UI.UpdateLot)
//	})
//	r.Route("/ws/", func(r chi.Router) {
//		r.HandleFunc("/lots", h.WS.WSLotsUpdate)
//		r.HandleFunc("/lot", h.WS.WSLotUpdate)
//	})
//	srv := &http.Server{Addr: ":5000", Handler: r}
//	//graceful
//	stopAppCh := make(chan struct{})
//	sigquit := make(chan os.Signal, 1)
//	stopLotCheck := make(chan struct{})
//	signal.Ignore(syscall.SIGHUP, syscall.SIGPIPE)
//	signal.Notify(sigquit, syscall.SIGINT, syscall.SIGTERM)
//	ticker := time.NewTicker(time.Second)
//	go func() {
//		s := <-sigquit
//		fmt.Printf("captured signal: %v\n", s)
//		err := closeDB()
//		if err != nil {
//			log.Printf("unable to close DB: %v", err)
//		}
//		for _, conn := range h.WS.WSLotClients.WsConn {
//			_ = conn.Close()
//		}
//		for _, conn := range h.WS.WSLotsClients.WsConn {
//			_ = conn.Close()
//		}
//		if err = srv.Shutdown(context.Background()); err != nil {
//			log.Fatalf("could not shutdown server: %s", err)
//		}
//		fmt.Println("shut down gracefully")
//		stopLotCheck <- struct{}{}
//		close(stopLotCheck)
//		stopAppCh <- struct{}{}
//	}()
//	go func() {
//		for {
//			select {
//			case <-ticker.C:
//				ll, err := h.Inter.InterService.EndTrades()
//				if err != nil {
//					log.Println(err)
//				}
//				for _, l := range ll {
//					h.WS.WSLotClients.BroadcastMessage(l)
//					h.WS.WSLotsClients.BroadcastMessage(l)
//				}
//			case <-stopLotCheck:
//				ticker.Stop()
//				return
//			}
//		}
//	}()
//	if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
//		log.Fatal(err)
//	}
//	<-stopAppCh
//}
