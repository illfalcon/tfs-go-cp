package main

//
//import (
//	"github.com/gorilla/websocket"
//	"gitlab.com/illfalcon/tfs-go-cp/internal/auth"
//	"gitlab.com/illfalcon/tfs-go-cp/internal/interactions"
//	"gitlab.com/illfalcon/tfs-go-cp/internal/lotsquery"
//	"gitlab.com/illfalcon/tfs-go-cp/internal/postgres"
//	"gitlab.com/illfalcon/tfs-go-cp/internal/sockets"
//	"gitlab.com/illfalcon/tfs-go-cp/internal/ui"
//)
//
//func main() {
//	tmpls := ui.ParseTemplates()
//	ccLots := sockets.WSClients{}
//	ccLot := sockets.WSClients{}
//	us, ss, ls, closeDB := postgres.CreateServices()
//	as := auth.MyAuthenticator{UserService: &us, SessionService: &ss}
//	ah := AuthHandler{AuthService: as}
//	qs := lotsquery.MyQueryService{UserService: &us, LotService: &ls, SessionService: &ss}
//	qh := QueryHandler{UserQueryService: qs}
//	is := interactions.MyInteractionService{UserService: &us, SessionService: &ss, LotService: &ls}
//	ih := InteractionsHandler{InterService: &is, WSLotClients: &ccLot, WSLotsClients: &ccLots}
//	uis := ui.MyUIService{Templates: tmpls, LotService: &ls, SessionService: &ss}
//	uih := UIHandler{UIService: uis}
//	upgrader := websocket.Upgrader{
//		ReadBufferSize:  4096,
//		WriteBufferSize: 4096,
//	}
//	wsh := WSHandler{Upgrader: upgrader, WSLotClients: &ccLot, WSLotsClients: &ccLots}
//	h := Handlers{Auth: ah, Query: qh, Inter: ih, UI: uih, WS: &wsh}
//	h.Start(closeDB)
//}
