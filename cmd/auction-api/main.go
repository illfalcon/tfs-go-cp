package main

import (
	"log"
	"net"

	lotsinteractionspb "gitlab.com/illfalcon/tfs-go-cp/internal/rpc/lotsinteractions"

	"gitlab.com/illfalcon/tfs-go-cp/internal/lotsinteractions"

	lotsquerypb "gitlab.com/illfalcon/tfs-go-cp/internal/rpc/lotsquery"

	"google.golang.org/grpc"

	"gitlab.com/illfalcon/tfs-go-cp/internal/lotsquery"
	"gitlab.com/illfalcon/tfs-go-cp/internal/postgres"
)

func main() {
	_, _, ls, cl := postgres.CreateServices()
	stopLotChan := make(chan struct{})
	qs := lotsquery.MyQueryService{LotService: &ls}
	is := lotsinteractions.MyLotsInteractionsService{LotService: &ls}
	s := server{QueryService: qs, InterService: &is, stopLotCheck: stopLotChan}
	lis, err := net.Listen("tcp", "localhost:8080")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
		_ = cl()
	}
	grpcServer := grpc.NewServer()
	lotsquerypb.RegisterLotsQueryServiceServer(grpcServer, &s)
	lotsinteractionspb.RegisterLotsInteractionsServiceServer(grpcServer, &s)
	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("can't register service server: %v", err)
		_ = cl()
	}
}
