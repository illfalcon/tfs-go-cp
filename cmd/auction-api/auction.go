package main

import (
	"context"
	"log"
	"time"

	interactions "gitlab.com/illfalcon/tfs-go-cp/internal/lotsinteractions"
	lotsinteractionspb "gitlab.com/illfalcon/tfs-go-cp/internal/rpc/lotsinteractions"

	"github.com/pkg/errors"
	query "gitlab.com/illfalcon/tfs-go-cp/internal/lotsquery"
	lotsquerypb "gitlab.com/illfalcon/tfs-go-cp/internal/rpc/lotsquery"
)

type server struct {
	QueryService query.Service
	InterService interactions.Service
	stopLotCheck chan struct{}
}

func (s *server) Lot(ctx context.Context, req *lotsquerypb.LotRequest) (*lotsquerypb.LotsResponse, error) {
	id := req.Id
	l, err := s.QueryService.Lot(int(id))
	if err != nil {
		return nil, errors.Wrap(err, "error in auction_api.Lot()")
	}
	resp := &lotsquerypb.LotsResponse{
		Lot: l,
	}
	return resp, nil
}

func (s *server) Lots(req *lotsquerypb.LotsRequest, stream lotsquerypb.LotsQueryService_LotsServer) error {
	status := req.Status
	ll, err := s.QueryService.Lots(status)
	if err != nil {
		return errors.Wrap(err, "error in auction_api.Lots()")
	}
	for _, l := range ll {
		resp := lotsquerypb.LotsResponse{
			Lot: l,
		}
		err = stream.Send(&resp)
		if err != nil {
			return errors.Wrap(err, "error in auction_api.Lots()")
		}
	}
	return nil
}

func (s *server) LotsOfUser(req *lotsquerypb.LotsOfUserRequest, stream lotsquerypb.LotsQueryService_LotsOfUserServer) error {
	userID := req.UserId
	lType := req.LotType
	ll, err := s.QueryService.LotsOfUser(int(userID), lType)
	if err != nil {
		return errors.Wrap(err, "error in auction_api.LotsOfUser()")
	}
	for _, l := range ll {
		resp := lotsquerypb.LotsResponse{
			Lot: l,
		}
		err = stream.Send(&resp)
		if err != nil {
			return errors.Wrap(err, "error in auction_api.LotsOfUser()")
		}
	}
	return nil
}

func (s *server) NewLot(ctx context.Context, req *lotsinteractionspb.NewLotRequest) (*lotsinteractionspb.LotsResponse, error) {
	endAt, _ := time.Parse(time.RFC3339, req.EndAt)
	l, err := s.InterService.NewLot(req.Title, req.Description, req.MinPrice, req.PriceStep, endAt, int(req.CreatorId))
	if err != nil {
		return nil, errors.Wrap(err, "error in auction_api.NewLot()")
	}
	resp := &lotsinteractionspb.LotsResponse{
		Lot: l,
	}
	return resp, nil
}

func (s *server) UpdateLot(ctx context.Context, req *lotsinteractionspb.UpdateLotRequest) (*lotsinteractionspb.LotsResponse, error) {
	endAt, _ := time.Parse(time.RFC3339, req.EndAt)
	l, err := s.InterService.UpdateLot(int(req.Lot_ID), req.Title, req.Description, req.MinPrice, req.PriceStep, req.Status, endAt)
	if err != nil {
		return nil, errors.Wrap(err, "error in auction_api.UpdateLot()")
	}
	resp := &lotsinteractionspb.LotsResponse{
		Lot: l,
	}
	return resp, nil
}

func (s *server) BuyLot(ctx context.Context, req *lotsinteractionspb.BuyLotRequest) (*lotsinteractionspb.LotsResponse, error) {
	l, err := s.InterService.BuyLot(int(req.Lot_ID), req.Price, int(req.Buyer_ID))
	if err != nil {
		return nil, errors.Wrap(err, "error in auction_api.BuyLot()")
	}
	resp := &lotsinteractionspb.LotsResponse{Lot: l}
	return resp, nil
}

func (s *server) EndLots(req *lotsinteractionspb.EmptyRequest, stream lotsinteractionspb.LotsInteractionsService_EndLotsServer) error {
	ll, err := s.InterService.EndLots()
	if err != nil {
		log.Println(err)
	}
	for _, l := range ll {
		resp := lotsinteractionspb.LotsResponse{
			Lot: l,
		}
		err = stream.Send(&resp)
		if err != nil {
			return errors.Wrap(err, "error in auction_api.EndLots()")
		}
	}
	return nil
}

func (s *server) DeleteLot(ctx context.Context, req *lotsinteractionspb.DeleteLotRequest) (*lotsinteractionspb.EmptyResponse, error) {
	err := s.InterService.DeleteLot(int(req.LotId), int(req.DeleterId))
	if err != nil {
		return nil, errors.Wrap(err, "error in auction_api.DeleteLot()")
	}
	return &lotsinteractionspb.EmptyResponse{}, nil
}
