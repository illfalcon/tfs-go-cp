package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/illfalcon/tfs-go-cp/internal/sockets"
	"gitlab.com/illfalcon/tfs-go-cp/internal/ui"

	"gitlab.com/illfalcon/tfs-go-cp/internal/lotsgateway"

	"gitlab.com/illfalcon/tfs-go-cp/internal/userquery"

	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"gitlab.com/illfalcon/tfs-go-cp/internal/auth"
	"gitlab.com/illfalcon/tfs-go-cp/internal/validation"
	"gitlab.com/illfalcon/tfs-go-cp/pkg/weberrors"
)

type AuthHandler struct {
	AuthService auth.Service
}

func switchErrors(w http.ResponseWriter, err error) {
	var e weberrors.Err
	switch errors.Cause(err) {
	case weberrors.ErrBadRequest:
		e.Error = err.Error()
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusBadRequest)
		return
	case weberrors.ErrForbidden:
		e.Error = errors.Cause(err).Error()
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusForbidden)
	case weberrors.ErrUnauthorized:
		e.Error = errors.Cause(err).Error()
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusUnauthorized)
	case weberrors.ErrNotFound:
		e.Error = errors.Cause(err).Error()
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusNotFound)
	case weberrors.ErrConflict:
		e.Error = errors.Cause(err).Error()
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusConflict)
	default:
		e.Error = errors.Cause(err).Error()
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusInternalServerError)
		return
	}
}

func (ah AuthHandler) SignUp(w http.ResponseWriter, r *http.Request) {
	var u validation.NewUser
	err := validation.Decode(r, &u)
	if err != nil {
		log.Println(fmt.Sprintf("error while parsing request: %v", err))
		switchErrors(w, err)
	}
	err = ah.AuthService.Register(u.EMail, u.Password, u.FirstName, u.LastName, u.DecodedBirthday)
	if err != nil {
		switchErrors(w, err)
		log.Println(fmt.Sprintf("error while handling registration: %v", err))
		return
	}
	w.WriteHeader(http.StatusCreated)
	_, _ = w.Write([]byte(""))
}

func (ah AuthHandler) SignIn(w http.ResponseWriter, r *http.Request) {
	var u validation.SignInUser
	err := validation.Decode(r, &u)
	if err != nil {
		log.Println(fmt.Sprintf("error while parsing request: %v", err))
		switchErrors(w, err)
	}
	bearer, err := ah.AuthService.Authenticate(u.EMail, u.Password)
	if err != nil {
		switchErrors(w, err)
		log.Println(fmt.Sprintf("error while handling authentication: %v", err))
		return
	}
	resp := struct {
		TokenType    string `json:"token_type"`
		AccessString string `json:"access_token"`
	}{TokenType: "bearer", AccessString: bearer}
	jsonResp, err := json.Marshal(resp)
	if err != nil {
		log.Println(err)
		switchErrors(w, err)
		return
	}
	cookie := http.Cookie{Name: "bearer-token", Value: bearer, Expires: time.Now().Add(30 * 24 * time.Hour), Path: "/"}
	http.SetCookie(w, &cookie)
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(jsonResp)

}

func (ah AuthHandler) UpdateUser(w http.ResponseWriter, r *http.Request) {
	bearer := r.Header.Get("Authorization")
	userIDStr := chi.URLParam(r, "id")
	userID, err := strconv.Atoi(userIDStr)
	if err != nil {
		log.Println(fmt.Sprintf("error while parsing id: %v", userIDStr))
		var e weberrors.Err
		e.Error = "wrong id format"
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusBadRequest)
		return
	}
	if bearer == "" {
		log.Println(fmt.Sprintf("bearer token for user %v not provided", userID))
		var e weberrors.Err
		e.Error = "bearer token for user %v not provided"
		jsonErr, _ := json.Marshal(e)
		http.Error(w, fmt.Sprintf(string(jsonErr), userID), http.StatusUnauthorized)
		return
	}
	token := bearer[strings.Index(bearer, " ")+1:]
	var u validation.UpdateUser
	err = validation.Decode(r, &u)
	if err != nil {
		log.Println(fmt.Sprintf("error while parsing request: %v", err))
		switchErrors(w, err)
	}
	jsonUser, err := ah.AuthService.Update(userID, token, u.FirstName, u.LastName, u.DecodedBirthday)
	if err != nil {
		switchErrors(w, err)
		return
	}
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(jsonUser)
}

type QueryHandler struct {
	UserQueryService  userquery.Service
	LotGateWayService lotsgateway.Service
}

func (qh QueryHandler) user(w http.ResponseWriter, r *http.Request) []byte {
	bearer := r.Header.Get("Authorization")
	userIDStr := chi.URLParam(r, "id")
	userID, err := strconv.Atoi(userIDStr)
	if err != nil {
		log.Println(fmt.Sprintf("error while parsing id: %v", userIDStr))
		var e weberrors.Err
		e.Error = "wrong id format"
		jsonErr, _ := json.Marshal(e)
		http.Error(w, fmt.Sprintf(string(jsonErr), userID), http.StatusUnauthorized)
		return nil
	}
	if bearer == "" {
		log.Println(fmt.Sprintf("bearer token for user %v not provided", userID))
		var e weberrors.Err
		e.Error = "bearer token for user %v not provided"
		jsonErr, _ := json.Marshal(e)
		http.Error(w, fmt.Sprintf(string(jsonErr), userID), http.StatusUnauthorized)
		return nil
	}
	token := bearer[strings.Index(bearer, " ")+1:]
	user, err := qh.UserQueryService.User(userID, token)
	if err != nil {
		switchErrors(w, err)
		log.Println(fmt.Sprintf("error while querying for user %v: %v", userID, err))
		return nil
	}
	return user
}

func (qh QueryHandler) User(w http.ResponseWriter, r *http.Request) {
	user := qh.user(w, r)
	if user != nil {
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(user)
	}
}

func (qh QueryHandler) Lots(w http.ResponseWriter, r *http.Request) {
	bearer := r.Header.Get("Authorization")
	if bearer == "" {
		log.Println(fmt.Sprint("bearer token not provided"))
		var e weberrors.Err
		e.Error = "bearer token for user %v not provided"
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusUnauthorized)
		return
	}
	token := bearer[strings.Index(bearer, " ")+1:]
	statuses, ok := r.URL.Query()["status"]
	status := ""
	if ok {
		status = statuses[0]
	}
	lots, err := qh.LotGateWayService.Lots(token, status)
	if err != nil {
		switchErrors(w, err)
		log.Println(err)
	}
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(lots)

}

func (qh QueryHandler) LotsOfUser(w http.ResponseWriter, r *http.Request) {
	bearer := r.Header.Get("Authorization")
	userIDStr := chi.URLParam(r, "id")
	userID, err := strconv.Atoi(userIDStr)
	if err != nil {
		log.Println(fmt.Sprintf("error while parsing id: %v", userIDStr))
		var e weberrors.Err
		e.Error = "wrong id format"
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusBadRequest)
		return
	}
	if bearer == "" {
		log.Println(fmt.Sprintf("bearer token for user %v not provided", userID))
		var e weberrors.Err
		e.Error = "bearer not provided"
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusUnauthorized)
		return
	}
	token := bearer[strings.Index(bearer, " ")+1:]
	types, ok := r.URL.Query()["type"]
	lotType := ""
	if ok {
		lotType = types[0]
	}
	lots, err := qh.LotGateWayService.LotsOfUser(userID, token, lotType)
	if err != nil {
		switchErrors(w, err)
		log.Println(err)
	}
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(lots)

}

func (qh QueryHandler) Lot(w http.ResponseWriter, r *http.Request) {
	bearer := r.Header.Get("Authorization")
	lotIDStr := chi.URLParam(r, "id")
	lotID, err := strconv.Atoi(lotIDStr)
	if err != nil {
		log.Println(fmt.Sprintf("error while parsing id: %v", lotIDStr))
		var e weberrors.Err
		e.Error = "wrong id format"
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusBadRequest)
		return
	}
	if bearer == "" {
		log.Println(fmt.Sprint("bearer token for user not provided"))
		var e weberrors.Err
		e.Error = "bearer not provided"
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusUnauthorized)
		return
	}
	token := bearer[strings.Index(bearer, " ")+1:]
	lot, err := qh.LotGateWayService.Lot(lotID, token)
	if err != nil {
		switchErrors(w, err)
		log.Println(err)
		return
	}
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(lot)

}

type InteractionsHandler struct {
	InterService  lotsgateway.Service
	WSLotClients  *sockets.WSClients
	WSLotsClients *sockets.WSClients
}

func (ih InteractionsHandler) NewLot(w http.ResponseWriter, r *http.Request) {
	bearer := r.Header.Get("Authorization")
	token := bearer[strings.Index(bearer, " ")+1:]
	var l validation.NewLot
	err := validation.Decode(r, &l)
	if err != nil {
		log.Println(fmt.Sprintf("error while parsing request: %v", err))
		switchErrors(w, err)
	}
	jsonLot, err := ih.InterService.NewLot(l.Title, l.Description, l.MinPrice, l.PriceStep, l.DecodedEndAt, token)
	if err != nil {
		log.Println(err)
		switchErrors(w, err)
	}
	w.WriteHeader(http.StatusCreated)
	_, _ = w.Write(jsonLot)
}

func (ih InteractionsHandler) UpdateLot(w http.ResponseWriter, r *http.Request) {
	bearer := r.Header.Get("Authorization")
	lotIDStr := chi.URLParam(r, "id")
	lotID, err := strconv.Atoi(lotIDStr)
	if err != nil {
		log.Println(fmt.Sprintf("error while parsing lot id: %v", lotIDStr))
		var e weberrors.Err
		e.Error = "wrong id format"
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusBadRequest)
		return
	}
	if bearer == "" {
		log.Println("bearer token not provided")
		var e weberrors.Err
		e.Error = "bearer token for not provided"
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusUnauthorized)
		return
	}
	token := bearer[strings.Index(bearer, " ")+1:]
	var l validation.NewLot
	err = validation.Decode(r, &l)
	if err != nil {
		switchErrors(w, err)
		log.Println(err)
	}
	lot, err := ih.InterService.UpdateLot(lotID, l.Title, l.Description, l.MinPrice, l.PriceStep, l.Status, l.DecodedEndAt, token)
	if err != nil {
		switchErrors(w, err)
		log.Println(err)
	}
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(lot)
	log.Println(lot)
	ih.WSLotsClients.BroadcastMessage(lot)
	ih.WSLotClients.BroadcastMessage(lot)
}

func (ih InteractionsHandler) BuyLot(w http.ResponseWriter, r *http.Request) {
	bearer := r.Header.Get("Authorization")
	lotIDStr := chi.URLParam(r, "id")
	lotID, err := strconv.Atoi(lotIDStr)
	if err != nil {
		log.Println(fmt.Sprintf("error while parsing lot id: %v", lotIDStr))
		var e weberrors.Err
		e.Error = "wrong id format"
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusBadRequest)
		return
	}
	if bearer == "" {
		log.Println("bearer token not provided")
		var e weberrors.Err
		e.Error = "bearer token for not provided"
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusUnauthorized)
		return
	}
	token := bearer[strings.Index(bearer, " ")+1:]
	var buyLot validation.BuyLot
	err = validation.Decode(r, &buyLot)
	if err != nil {
		switchErrors(w, err)
		log.Println(err)
	}
	l, err := ih.InterService.BuyLot(lotID, buyLot.Price, token)
	if err != nil {
		log.Println(err)
		switchErrors(w, err)
	}
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(l)
	ih.WSLotsClients.BroadcastMessage(l)
	ih.WSLotClients.BroadcastMessage(l)
}

func (ih InteractionsHandler) DeleteLot(w http.ResponseWriter, r *http.Request) {
	bearer := r.Header.Get("Authorization")
	lotIDStr := chi.URLParam(r, "id")
	lotID, err := strconv.Atoi(lotIDStr)
	if err != nil {
		log.Println(fmt.Sprintf("error while parsing lot id: %v", lotIDStr))
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if bearer == "" {
		log.Println("bearer token not provided")
		http.Error(w, "bearer token for user %v not provided", http.StatusBadRequest)
		return
	}
	token := bearer[strings.Index(bearer, " ")+1:]
	err = ih.InterService.DeleteLot(lotID, token)
	if err != nil {
		switchErrors(w, err)
		log.Println(fmt.Sprintf("error while handling lot deletion: %v", err))
		return
	}
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte(""))
}

type UIHandler struct {
	UIService ui.Service
}

func (ui UIHandler) Lots(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("bearer-token")
	if err != nil {
		log.Println(fmt.Sprint("bearer token not provided"))
		var e weberrors.Err
		e.Error = "bearer token for not provided"
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusUnauthorized)
		return
	}
	bearer := cookie.Value
	if bearer == "" {
		log.Println(fmt.Sprint("bearer token not provided"))
		var e weberrors.Err
		e.Error = "bearer token for not provided"
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusUnauthorized)
		return
	}
	statuses, ok := r.URL.Query()["status"]
	status := ""
	if ok {
		status = statuses[0]
	}
	err = ui.UIService.RenderLots(w, status, bearer)
	if err != nil {
		log.Println(err)
		switchErrors(w, err)
	}
}

func (ui UIHandler) Lot(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("bearer-token")
	if err != nil {
		log.Println(fmt.Sprint("bearer token not provided"))
		var e weberrors.Err
		e.Error = "bearer token for not provided"
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusUnauthorized)
		return
	}
	bearer := cookie.Value
	if bearer == "" {
		log.Println(fmt.Sprint("bearer token not provided"))
		var e weberrors.Err
		e.Error = "bearer token for not provided"
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusUnauthorized)
		return
	}
	lotIDStr := chi.URLParam(r, "id")
	lotID, err := strconv.Atoi(lotIDStr)
	if err != nil {
		log.Println(fmt.Sprintf("error while parsing id: %v", lotIDStr))
		var e weberrors.Err
		e.Error = "bearer token for not provided"
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusUnauthorized)
		return
	}
	err = ui.UIService.RenderLot(w, lotID, bearer)
	if err != nil {
		log.Println(err)
		switchErrors(w, err)
	}
}

func (ui UIHandler) SignIn(w http.ResponseWriter, r *http.Request) {
	err := ui.UIService.RenderSignIn(w)
	if err != nil {
		log.Println(err)
		switchErrors(w, err)
	}
}

func (ui UIHandler) NewLot(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("bearer-token")
	if err != nil {
		log.Println(fmt.Sprint("bearer token not provided"))
		var e weberrors.Err
		e.Error = "bearer token for not provided"
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusUnauthorized)
		return
	}
	bearer := cookie.Value
	if bearer == "" {
		log.Println(fmt.Sprint("bearer token not provided"))
		var e weberrors.Err
		e.Error = "bearer token for not provided"
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusUnauthorized)
		return
	}
	err = ui.UIService.RenderNewLot(w, bearer)
	if err != nil {
		log.Println(err)
		switchErrors(w, err)
	}
}

func (ui UIHandler) UpdateLot(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("bearer-token")
	if err != nil {
		log.Println(fmt.Sprint("bearer token not provided"))
		var e weberrors.Err
		e.Error = "bearer token for not provided"
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusUnauthorized)
		return
	}
	bearer := cookie.Value
	if bearer == "" {
		log.Println(fmt.Sprint("bearer token not provided"))
		var e weberrors.Err
		e.Error = "bearer token for not provided"
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusUnauthorized)
		return
	}
	lotIDStr := chi.URLParam(r, "id")
	lotID, err := strconv.Atoi(lotIDStr)
	if err != nil {
		log.Println(fmt.Sprintf("error while parsing id: %v", lotIDStr))
		var e weberrors.Err
		e.Error = "wrong id format"
		jsonErr, _ := json.Marshal(e)
		http.Error(w, string(jsonErr), http.StatusBadRequest)
		return
	}
	err = ui.UIService.RenderUpdateLot(w, lotID, bearer)
	if err != nil {
		log.Println(err)
		switchErrors(w, err)
	}
}

type WSHandler struct {
	Upgrader      websocket.Upgrader
	WSLotsClients *sockets.WSClients
	WSLotClients  *sockets.WSClients
}

func (ws *WSHandler) WSLotsUpdate(w http.ResponseWriter, r *http.Request) {
	conn, err := ws.Upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Printf("can't upgrade connection: %s\n", err)
		return
	}
	ws.WSLotsClients.AddClient(conn)
}

func (ws *WSHandler) WSLotUpdate(w http.ResponseWriter, r *http.Request) {
	conn, err := ws.Upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Printf("can't upgrade connection: %s\n", err)
		return
	}
	ws.WSLotClients.AddClient(conn)
}

type Handlers struct {
	Auth  AuthHandler
	Query QueryHandler
	Inter InteractionsHandler
	UI    UIHandler
	WS    *WSHandler
}

func (h Handlers) Start(cl func() error) {
	r := chi.NewRouter()
	r.Route("/v1/auction/", func(r chi.Router) {
		r.Post("/signup", h.Auth.SignUp)
		r.Post("/signin", h.Auth.SignIn)
		r.Put("/users/{id}", h.Auth.UpdateUser)
		r.Get("/users/{id}", h.Query.User)
		r.Post("/lots", h.Inter.NewLot)
		r.Put("/lots/{id}", h.Inter.UpdateLot)
		r.Get("/lots/{id}", h.Query.Lot)
		r.Delete("/lots/{id}", h.Inter.DeleteLot)
		r.Get("/users/{id}/lots", h.Query.LotsOfUser)
		r.Get("/lots", h.Query.Lots)
		r.Put("/lots/{id}/buy", h.Inter.BuyLot)
	})
	r.Route("/", func(r chi.Router) {
		r.Get("/lots", h.UI.Lots)
		r.Get("/lots/{id}", h.UI.Lot)
		r.Get("/signin", h.UI.SignIn)
		r.Get("/lots/new", h.UI.NewLot)
		r.Get("/lots/{id}/update", h.UI.UpdateLot)
	})
	r.Route("/ws/", func(r chi.Router) {
		r.HandleFunc("/lots", h.WS.WSLotsUpdate)
		r.HandleFunc("/lot", h.WS.WSLotUpdate)
	})
	srv := &http.Server{Addr: ":5000", Handler: r}
	stopAppCh := make(chan struct{})
	sigquit := make(chan os.Signal, 1)
	signal.Ignore(syscall.SIGHUP, syscall.SIGPIPE)
	signal.Notify(sigquit, syscall.SIGINT, syscall.SIGTERM)
	ticker := time.NewTicker(time.Second)
	stopLotCheck := make(chan struct{})
	go func() {
		s := <-sigquit
		fmt.Printf("captured signal: %v\n", s)
		//for _, conn := range h.WS.WSLotClients.WsConn {
		//	_ = conn.Close()
		//}
		//for _, conn := range h.WS.WSLotsClients.WsConn {
		//	_ = conn.Close()
		//}
		if err := srv.Shutdown(context.Background()); err != nil {
			log.Fatalf("could not shutdown server: %s", err)
		}
		fmt.Println("shut down gracefully")
		stopLotCheck <- struct{}{}
		close(stopLotCheck)
		_ = cl()
		stopAppCh <- struct{}{}
	}()
	go func() {
		for {
			select {
			case <-ticker.C:
				ll, err := h.Inter.InterService.EndTrades()
				if err != nil {
					log.Println(err)
				}
				for _, l := range ll {
					h.WS.WSLotClients.BroadcastMessage(l)
					h.WS.WSLotsClients.BroadcastMessage(l)
				}
			case <-stopLotCheck:
				ticker.Stop()
				return
			}
		}
	}()
	if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatal(err)
	}
	<-stopAppCh
}
