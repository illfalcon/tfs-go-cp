package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/pkg/errors"

	"gitlab.com/illfalcon/tfs-go-cp/internal/validation"

	"gitlab.com/illfalcon/tfs-go-cp/internal/auth"
	"gitlab.com/illfalcon/tfs-go-cp/internal/inmemorydb"
)

func TestAuthHandler_SignUp(t *testing.T) {
	us := inmemorydb.NewUserService()
	ss := inmemorydb.NewSessionService()
	as := auth.MyAuthenticator{UserService: &us, SessionService: &ss}
	ah := AuthHandler{AuthService: as}
	newUser := validation.NewUser{FirstName: "Hello", LastName: "Test", EMail: "email@test.go", Password: "testtest"}
	newUserJSON, err := json.Marshal(newUser)
	if err != nil {
		t.Fatal(errors.Wrap(err, "error in json"))
	}
	postData := strings.NewReader(string(newUserJSON))
	req := httptest.NewRequest(http.MethodPost, "/signup", postData)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(ah.SignUp)
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expected := 1
	if len(us.Storage) != expected {
		t.Errorf("handler returned Storage size: got %v want %v",
			rr.Body.String(), expected)
	}
}
