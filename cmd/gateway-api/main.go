package main

import (
	"log"

	"github.com/gorilla/websocket"
	"gitlab.com/illfalcon/tfs-go-cp/internal/sockets"
	"gitlab.com/illfalcon/tfs-go-cp/internal/ui"

	lotsinteractionspb "gitlab.com/illfalcon/tfs-go-cp/internal/rpc/lotsinteractions"

	"gitlab.com/illfalcon/tfs-go-cp/internal/lotsgateway"

	"gitlab.com/illfalcon/tfs-go-cp/internal/auth"

	"gitlab.com/illfalcon/tfs-go-cp/internal/postgres"
	lotsquerypb "gitlab.com/illfalcon/tfs-go-cp/internal/rpc/lotsquery"
	"gitlab.com/illfalcon/tfs-go-cp/internal/userquery"
	"google.golang.org/grpc"
)

func main() {
	tmpls := ui.ParseTemplates()
	ccLots := sockets.WSClients{}
	ccLot := sockets.WSClients{}
	us, ss, _, close := postgres.CreateServices()
	conn, err := grpc.Dial("localhost:8080", grpc.WithInsecure())
	if err != nil {
		log.Fatal("cannot connect to auction-api")
	}
	defer conn.Close()
	queryClient := lotsquerypb.NewLotsQueryServiceClient(conn)
	interClient := lotsinteractionspb.NewLotsInteractionsServiceClient(conn)
	uqs := userquery.MyUserQueryService{UserService: &us, SessionService: &ss}
	lgs := lotsgateway.MyLotsGatewayService{SessionService: &ss, GRPCQueryClient: queryClient, GRPCInterClient: interClient}
	qh := QueryHandler{UserQueryService: uqs, LotGateWayService: lgs}
	as := auth.MyAuthenticator{UserService: &us, SessionService: &ss}
	ah := AuthHandler{AuthService: as}
	ih := InteractionsHandler{InterService: lgs, WSLotClients: &ccLot, WSLotsClients: &ccLots}
	uis := ui.MyUIService{Templates: tmpls, LotService: &lgs, SessionService: &ss}
	uih := UIHandler{UIService: uis}
	upgrader := websocket.Upgrader{
		ReadBufferSize:  4096,
		WriteBufferSize: 4096,
	}
	wsh := WSHandler{Upgrader: upgrader, WSLotClients: &ccLot, WSLotsClients: &ccLots}
	h := Handlers{Auth: ah, Query: qh, Inter: ih, UI: uih, WS: &wsh}
	h.Start(close)
}
